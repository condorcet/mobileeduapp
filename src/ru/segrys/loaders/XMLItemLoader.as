package ru.segrys.loaders 
{
	import flash.utils.ByteArray;
	import ru.segrys.caches.AbstractCache;
	import ru.segrys.events.loader.LoaderEvent;
	
	/**
	 * ...
	 * @author ...
	 */
	public class XMLItemLoader extends ItemLoader 
	{
		
		public function XMLItemLoader(cachable:Boolean=false, c:AbstractCache=null) 
		{
			super(cachable, c);
			
		}
		
		override protected function serializeData(ba:ByteArray):void
		{
			/*var tmpBa:ByteArray = new ByteArray();
			tmpBa.readBytes(ba, 3, ba.length);*/
			var tmp:String = new String(ba);
			data = new XML(ba);
			var event:LoaderEvent = new LoaderEvent(LoaderEvent.SERIALIZED);
			dispatchEvent(event);
		}
		
	}

}