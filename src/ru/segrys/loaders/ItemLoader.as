package ru.segrys.loaders 
{
	import flash.events.HTTPStatusEvent;
	import flash.events.SecurityErrorEvent;
	import ru.segrys.caches.AbstractCache;
	import flash.display.BitmapData;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.net.URLRequest;
	import flash.events.IOErrorEvent;
	import flash.net.URLLoader;
	import flash.net.URLLoaderDataFormat;
	import ru.segrys.events.loader.LoaderEvent;
	import ru.segrys.events.cache.ItemCacheEvent;
	import ru.segrys.caches.ICache;
	import ru.segrys.caches.AbstractCache;
	import flash.utils.ByteArray;
	import flash.net.URLRequestMethod;
	/**
	 * Класс загрузчика.
	 * Данные сохраняются в поле data как байтовый массив (ByteArray)
	 * Поддерживает кэширование данных, для этого cachable должно быть true, а в поле cache - указатель на кэш @see ru.segrys.caches.AbstractCache
	 * Метод serializeData может быть переопределен для приведения загруженных данных к определенному формату/типу/классу
	 * @author Novikov
	 */
	public class ItemLoader extends EventDispatcher
	{
		/**
		 * Загруженные данные
		 */
		public var data:Object;
		/**
		 * Использованный для загрузки данных запрос
		 */
		public var request:URLRequest;
		/**
		 * Флаг кэширования
		 */
		public var cachable:Boolean;
		
		/**
		 * Кэш данных
		 */
		private var cache:AbstractCache;
		/**
		 * Загрузчик класса URLLoader
		 */
		private var loader:URLLoader;
		
		public function ItemLoader(cachable:Boolean = false, c:AbstractCache = null) 
		{
			this.cachable = cachable;
			cache = c;
			if (cachable && cache == null)
				throw new Error('cachable is true, but cache = null');
			if (cachable)
			{
				cache.addEventListener(ItemCacheEvent.ITEM_LOADED, cacheLoadedHandler);
				cache.addEventListener(ItemCacheEvent.ITEM_NOT_LOADED, cacheErrorHandler); //не получилось загрузить из кэша
				cache.addEventListener(ItemCacheEvent.ITEM_NOT_EXISTS, cacheErrorHandler); //не получилось загрузить из кэша
			}
			loader = new URLLoader();
			loader.dataFormat = URLLoaderDataFormat.BINARY;
			loader.addEventListener(Event.COMPLETE, loaderCompleteHandler);
			loader.addEventListener(IOErrorEvent.IO_ERROR, loaderErrorHandler);
			loader.addEventListener(SecurityErrorEvent.SECURITY_ERROR, securityErrorHandler);
			//loader.addEventListener(HTTPStatusEvent.HTTP_RESPONSE_STATUS, securityErrorHandler);
			addEventListener(LoaderEvent.SERIALIZED, dataSerializedHandler);
		}
		/**
		 * Метод загрузки данных
		 * @param	request URL запрос
		 */
		public function load(request:URLRequest):void
		{
			this.request = request;
			//this.request.method = URLRequestMethod.POST;
			if (!cachable)// || !cache.hasItem(request))
				loader.load(request);
			else
				cache.load(request.url);
		}
		
		private function loaderCompleteHandler(e:Event):void
		{
			serializeData(loader.data);
		}
		private function loaderErrorHandler(e:IOErrorEvent):void
		{
			data = null;
			var event:LoaderEvent = new LoaderEvent(LoaderEvent.ERROR);
			dispatchEvent(event);
		}
		private function securityErrorHandler(e:SecurityErrorEvent):void
		{
			trace('security error');
		}
		private function cacheLoadedHandler(e:ItemCacheEvent):void
		{
			if(request && e.key == request.url)
				serializeData(cache.data);
		}
		private function cacheErrorHandler(e:ItemCacheEvent):void
		{
			if (!request || e.key != request.url)
				return;
			loader.addEventListener(Event.COMPLETE, cacheCompleteHandler);
			loader.load(request);
			function cacheCompleteHandler(e:Event):void
			{
				cache.saveItem(loader.data,request.url);
				loader.removeEventListener(Event.COMPLETE, cacheCompleteHandler);
			}
		}
		private function dataSerializedHandler(e:LoaderEvent):void
		{
			var event:LoaderEvent = new LoaderEvent(LoaderEvent.COMPLETE);
			dispatchEvent(event);
		}
		/**
		 * Метод сериализации данных (т.е. приведения к нужному формату/типу/классу)
		 * @param	ba данные
		 */
		protected function serializeData(ba:ByteArray):void
		{
			var event:LoaderEvent = new LoaderEvent(LoaderEvent.SERIALIZED);
			dispatchEvent(event);
		}
	}

}