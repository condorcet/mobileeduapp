package ru.segrys.loaders 
{
	import flash.display.BitmapData;
	import flash.display.Bitmap;
	import flash.display.Loader;
	import flash.events.Event;
	import flash.system.LoaderContext;
	import flash.utils.ByteArray;
	import ru.segrys.events.loader.LoaderEvent;
	import ru.segrys.caches.AbstractCache;
		/**
	 * Класс загрузчика SWF-ок
	 * @author Novikov
	 */
	public class SWFItemLoader extends ItemLoader 
	{
		public function SWFItemLoader(cachable:Boolean = false, c:AbstractCache = null) 
		{
			super(cachable, c);
		}
		override protected function serializeData(ba:ByteArray):void
		{
			var swfLoader:Loader = new Loader();
			swfLoader.contentLoaderInfo.addEventListener(Event.COMPLETE, completeSerialized);
			var context:LoaderContext = new LoaderContext();
			context.allowCodeImport = true;
			swfLoader.loadBytes(ba, context);
			function completeSerialized(e:Event):void
			{
				data = swfLoader.content;
				var event:LoaderEvent = new LoaderEvent(LoaderEvent.SERIALIZED);
				dispatchEvent(event);
			}
			
		}
	}

}