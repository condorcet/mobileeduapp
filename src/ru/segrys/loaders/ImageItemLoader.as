package ru.segrys.loaders 
{
	import flash.display.BitmapData;
	import flash.display.Bitmap;
	import flash.display.Loader;
	import flash.events.Event;
	import flash.utils.ByteArray;
	import ru.segrys.events.loader.LoaderEvent;
	import ru.segrys.caches.AbstractCache;
		/**
	 * Класс загрузчика изображения
	 * @author Novikov
	 */
	public class ImageItemLoader extends ItemLoader 
	{
		public function ImageItemLoader(cachable:Boolean = false, c:AbstractCache = null) 
		{
			super(cachable, c);
		}
		override protected function serializeData(ba:ByteArray):void
		{
			var imageLoader:Loader = new Loader();
			imageLoader.contentLoaderInfo.addEventListener(Event.COMPLETE, completeSerialized);
			imageLoader.loadBytes(ba);
			function completeSerialized(e:Event):void
			{
				data = imageLoader.content;
				var event:LoaderEvent = new LoaderEvent(LoaderEvent.SERIALIZED);
				dispatchEvent(event);
			}
			
		}
	}

}