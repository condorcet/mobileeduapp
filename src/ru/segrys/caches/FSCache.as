package ru.segrys.caches 
{
	import flash.events.OutputProgressEvent;
	import ru.segrys.events.cache.CacheEvent;
	import ru.segrys.events.cache.ItemCacheEvent;
	import flash.errors.IOError;
	import flash.events.DataEvent;
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.filesystem.File;
	import flash.filesystem.FileStream;
	import flash.filesystem.FileMode;
	import flash.utils.Dictionary;
	import flash.utils.ByteArray;
	/** 
	* Событие будет вызвано в случае отстуствия файла кэш-таблицы или директории кэша
	* @eventType ru.segrys.events.cache.CacheEvent.CACHE_NOT_EXISTS
	*/ 
	[Event(name = "cacheNotExist", type = "ru.segrys.events.cache.CacheEvent")]
	/**
	 * Событие будет вызвано в любом случае некорректного выполнения операций над кэшом
	 * @eventType ru.segrys.events.cache.CacheEvent.CACHE_ERROR
	 */
	[Event(name = "cacheError", type = "ru.segrys.events.cache.CacheEvent")]
	/**
	 * Событие будет вызвано при ошибках в операциях над кэш-таблицей
	 * @eventType ru.segrys.events.cache.CacheEvent.CACHETABLE_ERROR
	 */
	[Event(name = "cacheTableError", type = "ru.segrys.events.cache.CacheEvent")]
	/**
	 * Событие будет вызвано при создании кэш-таблицы в файловой системе
	 * @eventType ru.segrys.events.cache.CacheEvent.CACHETABLE_CREATE
	 */
	[Event(name = "cacheTableCreate", type = "ru.segrys.events.cache.CacheEvent")]
	/**
	 * Событие будет вызвано после успешной загрузки кэш-таблицы (при условии наличия директории кэша)
	 * @eventType ru.segrys.events.cache.CacheEvent.CACHE_LOADED
	 */
	[Event(name = "cacheLoaded", type = "ru.segrys.events.cache.CacheEvent")]
	/**
	 * Событие будет вызвано после неуспешной загрузки кэша/кэш-таблицы
	 * @eventType ru.segrys.events.cache.CacheEvent.CACHE_NOT_LOADED
	 */
	[Event(name = "cacheNotLoaded", type = "ru.segrys.events.cache.CacheEvent")]
	/**
	 * Событие связано с отсутствием свободного места в файловой системе для сохранения данных
	 * @eventType ru.segrys.events.cache.CacheEvent.NOT_ENOUGH_SPACE
	 */
	[Event(name = "notEngoughSpace", type = "ru.segrys.events.cache.CacheEvent")]
	/**
	 * Событие будет вызвано после успешного создания кэша и кэш-таблицы
	 * @eventType ru.segrys.events.cache.CacheEvent.CREATED
	 */
	[Event(name = "cacheCreated", type = "ru.segrys.events.cache.CacheEvent")]
	/**
	 * Событие будет вызвано после очистки кэша
	 * @eventType ru.segrys.events.cache.CacheEvent.CLEARED
	 */
	[Event(name = "cacheCleared", type = "ru.segrys.events.cache.CacheEvent")]
	/**
	 * Событие будет вызвано после успешной загрузки данных из кэша
	 * @eventType ru.segrys.events.cache.ItemCacheEvent.ITEM_LOADED
	 */	
	[Event(name = "itemLoaded", type = "ru.segrys.events.cache.ItemCacheEvent")]
	/**
	 * Событие будет вызвано после неуспешной загрузки данных из кэша
	 * @eventType ru.segrys.events.cache.ItemCacheEvent.ITEM_NOT_LOADED
	 */
	[Event(name = "itemNotLoaded", type = "ru.segrys.events.cache.ItemCacheEvent")]
	/**
	 * Событие будет вызвано после успешного сохранения данных в кэш
	 * @eventType ru.segrys.events.cache.ItemCacheEvent.ITEM_SAVED
	 */
	[Event(name = "itemSaved", type = "ru.segrys.events.cache.ItemCacheEvent")]
	/**
	 * Событие будет вызвано после успешного сохранения данных в кэш
	 * @eventType ru.segrys.events.cache.ItemCacheEvent.ITEM_NOT_SAVED
	 */
	[Event(name = "itemNotSaved", type = "ru.segrys.events.cache.ItemCacheEvent")]
	/**
	 * Событие возникающее при отсутствии данных в файловой системе по запрашиваемому ключу из кэш-таблицы
	 * @eventType ru.segrys.events.cache.ItemCacheEvent.FILE_ITEM_NOT_EXISTS
	 */
	[Event(name = "fileItemNotExists", type = "ru.segrys.events.cache.ItemCacheEvent")]
	/**
	 * Событие возникающее при отсутствии данных в кэш-таблице
	 * @eventType ru.segrys.events.cache.ItemCacheEvent.FILE_ITEM_NOT_EXISTS
	 */
	[Event(name="itemNotExists", type="ru.segrys.events.cache.ItemCacheEvent")]
	/**
	 * Событие будет вызвано при неуспешном удалении данных из кэша
	 * @eventType ru.segrys.events.cache.ItemCacheEvent.ITEM_NOT_REMOVED
	 */
	[Event(name = "itemNotRemoved", type = "ru.segrys.events.cache.ItemCacheEvent")]
	/**
	 * Событие будет вызвано при успешном удалении данных из кэша
	 * @eventType ru.segrys.events.cache.ItemCacheEvent.ITEM_REMOVED
	 */
	[Event(name = "itemRemoved", type = "ru.segrys.events.cache.ItemCacheEvent")]
	
	/**
	 * Реализация кэша для локальной дисковой файловой системы.
	 * Описание основных методов см. в ru.segrys.caches.AbstractCache @see ru.segrys.caches.AbstractCache
	 * @author Novikov
	 */
	public class FSCache extends AbstractCache 
	{
		/**
		 * Кэш-таблица
		 */
		private var cacheTable:Dictionary;
		/**
		 * Указатель на файл кэш-таблицы в ФС
		 */
		private var cacheTableFile:File;
		/**
		 * Указатель на кэш (фактически директорию с кэшированными данными)
		 */
		private var pathToCache:File;
		/**
		 * Указатель в фс на данные (а нужно ли?)
		 */
		private var dataFile:File;
		/**
		 * Счетчик пройденных этапов создания кэша (см. метод createCache)
		 * максимальное значение 2 - пройден этап создания кэш-таблицы и директории кэша
		 */
		private var createdCounter:uint = 0;
		/**
		 * Режим удаления данных из кэша
		 * LAZY_MODE - отложенное удаление
		 * STRONG_MODE - немедленное удаление
		 */
		private var removingMode:String = LAZY_MODE;
		/**
		 * Список удаляемых данных из кэша в режиме LAZY_MODE
		 */
		private var listOfRemoving:Array = new Array();
		
		/**
		 * Режим удаления LAZY_MODE - удаление по вызову метода updateCache, данные накапливаются в списке listOfRemoving
		 */
		private static const LAZY_MODE:String = 'lazyMode';
		/**
		 * Режим удаления STRONG_MODE - удаление сразу после вызова соответсвующей команды
		 */
		private static const STRONG_MODE:String = 'strongMode';
		
		public function FSCache(pathToCache:File, cacheTableFile:File)
		{
			this.pathToCache = pathToCache;
			this.cacheTableFile = cacheTableFile;

		}
		/**
		*/
		override public function createCache():void
		{
			createCacheTable();
			createCacheDirectory();
		}
		override public function loadCache():void
		{
			loadCacheTable();
		}
		override public function clearCache():void
		{
			//очистить содержимое кэша
			cacheTable = new Dictionary();
			updateCache();
			pathToCache.deleteDirectoryAsync(true);
						
			pathToCache.addEventListener(Event.COMPLETE, removingComplete);
			pathToCache.addEventListener(IOErrorEvent.IO_ERROR, removingError);
			function removingComplete(e:Event):void
			{
				dispatchEvent(new CacheEvent(CacheEvent.CLEARED));
			}
			function removingError(e:Event):void
			{
				dispatchEvent(new CacheEvent(CacheEvent.CACHE_ERROR, 'cache clear error'));
			}
		}
		override public function updateCache():void
		{
			//сохранение кэш-таблицы в ФС
			var fileStream:FileStream = new FileStream();
			fileStream.addEventListener(IOErrorEvent.IO_ERROR, errorHandler);
			fileStream.addEventListener(Event.CLOSE, closeHandler);
			fileStream.openAsync(cacheTableFile, FileMode.WRITE);
			fileStream.writeObject(cacheTable);
			fileStream.close();
			if (removingMode == LAZY_MODE)
				removeItems();
			function errorHandler(e:IOErrorEvent):void
			{
				dispatchEvent(new CacheEvent(CacheEvent.CACHETABLE_ERROR,'cachetable saving error'));
				dispatchEvent(new CacheEvent(CacheEvent.CACHE_ERROR, 'cachetable saving error'));
			}
			function closeHandler(e:Event):void
			{
				//??
			}
		}
		override public function hasItem(key:Object):Object
		{
			//находятся ли данные по ключу в кэше
			return cacheTable[key];
		}
		override public function load(key:Object):void
		{
			this.key = key;
			data = null;
			if (hasItem(key) == null)
			{
				dispatchEvent(new ItemCacheEvent(ItemCacheEvent.ITEM_NOT_EXISTS, key));
				dispatchEvent(new CacheEvent(CacheEvent.NOT_LOADED));
				return;
			}
			var url:String = String(cacheTable[key]);
			trace(url);
			dataFile = pathToCache.resolvePath(url);
			if (!dataFile.exists)
			{
				dispatchEvent(new ItemCacheEvent(ItemCacheEvent.FILE_ITEM_NOT_EXISTS, key));
				dispatchEvent(new CacheEvent(CacheEvent.CACHE_NOT_EXISTS));
				return;
			}
			var fileStream:FileStream = new FileStream();
			fileStream.openAsync(dataFile, FileMode.READ);
			fileStream.addEventListener(Event.COMPLETE, itemLoaded);
			fileStream.addEventListener(IOErrorEvent.IO_ERROR, itemLoadedError);
			
			function itemLoaded(e:Event):void
			{
				removeListeners(); //а надо ли удалять?
				data = new ByteArray();
				//fileStream.position = 0;
				fileStream.readBytes(data);
				fileStream.close();
				//trace('from cache: '+String(data));
				dispatchEvent(new ItemCacheEvent(ItemCacheEvent.ITEM_LOADED, key));
			}
			function itemLoadedError(e:IOErrorEvent):void
			{
				removeListeners();
				dispatchEvent(new ItemCacheEvent(ItemCacheEvent.ITEM_NOT_LOADED, key));
			}
			function removeListeners():void
			{
				fileStream.removeEventListener(Event.COMPLETE, itemLoaded);
				fileStream.removeEventListener(IOErrorEvent.IO_ERROR, itemLoadedError);
			}
		}
		override public function saveItem(item:Object, key:Object):void
		{
			this.key = key;
			data = null;
			var ba:ByteArray = new ByteArray();
			ba.writeBytes(item as ByteArray);//writeObject(item);
			if (cacheTable[key] != null)
			{
				removeItem(String(cacheTable[key]),true); //удалять необходимо сразу
			}
			if (pathToCache.spaceAvailable > ba.length)
			{
				var fileStream:FileStream = new FileStream();
				var name:String;
				var file:File;
				//временная заглушка
				do
				{
					name = Number(Math.random() * 10000).toFixed(5); 
					file = pathToCache.resolvePath(name);
				}while (file.exists)
				fileStream.addEventListener(OutputProgressEvent.OUTPUT_PROGRESS, saveItemComplete);
				fileStream.addEventListener(IOErrorEvent.IO_ERROR, saveItemError);
				fileStream.openAsync(file, FileMode.WRITE);
				fileStream.writeBytes(ba);
				fileStream.close();
			}
			else
			{
				dispatchEvent(new CacheEvent(CacheEvent.CACHE_ERROR, 'not enough space'));
				dispatchEvent(new CacheEvent(CacheEvent.NOT_ENOUGH_SPACE));
				dispatchEvent(new ItemCacheEvent(ItemCacheEvent.ITEM_NOT_SAVED, key));
			}
			
			function saveItemComplete(e:OutputProgressEvent):void
			{
				if (e.bytesPending == 0)
				{
					cacheTable[key] = name;
					e.target.removeEventListener(OutputProgressEvent.OUTPUT_PROGRESS, saveItemComplete);
					e.target.removeEventListener(IOErrorEvent.IO_ERROR, saveItemError);
					e.target.close();
					dispatchEvent(new ItemCacheEvent(ItemCacheEvent.ITEM_SAVED, key));
				}
			}
			function saveItemError(e:IOErrorEvent):void
			{
				e.target.removeEventListener(OutputProgressEvent.OUTPUT_PROGRESS, saveItemComplete);
				e.target.removeEventListener(IOErrorEvent.IO_ERROR, saveItemError);
				dispatchEvent(new ItemCacheEvent(ItemCacheEvent.ITEM_NOT_SAVED));
				dispatchEvent(new CacheEvent(CacheEvent.CACHE_ERROR,'saving item error'));
			}
		}
		
		public function getCacheTable():Dictionary
		{
			return cacheTable;
		}
		
		//удаление элемента из кэша
		private function removeItem(path:String, ignoreMode:Boolean = false):void
		{
			if (removingMode == LAZY_MODE && !ignoreMode)
			{
				listOfRemoving.push(path);
				return;
			}
			var file:File = pathToCache.resolvePath(path);
			file.addEventListener(Event.COMPLETE, completeHandler);
			file.addEventListener(IOErrorEvent.IO_ERROR, errorHandler);
			file.deleteFileAsync();
			function completeHandler(e:Event):void
			{
				dispatchEvent(new ItemCacheEvent(ItemCacheEvent.ITEM_REMOVED, key));
			}
			function errorHandler(e:IOErrorEvent):void
			{	
				dispatchEvent(new ItemCacheEvent(ItemCacheEvent.ITEM_NOT_REMOVED, key));
			}
		}
		
		//удаление элементов согласно очереди удаления
		private function removeItems(e:Event = null):void
		{
			if (e == null)
			{
				addEventListener(ItemCacheEvent.ITEM_REMOVED, removeItems);
				addEventListener(ItemCacheEvent.ITEM_NOT_REMOVED, removeItems);
			}
			if (listOfRemoving.length == 0)
			{
				removeEventListener(ItemCacheEvent.ITEM_REMOVED, removeItems);
				removeEventListener(ItemCacheEvent.ITEM_NOT_REMOVED, removeItems);
				return;
			}
			var item:String = listOfRemoving.pop();
			removeItem(item,true);
		}
		
		private function createCacheTable():void
		{
			var fileStream:FileStream = new FileStream();
			
			fileStream.addEventListener(IOErrorEvent.IO_ERROR, errorHandler);
			fileStream.addEventListener(Event.CLOSE, closeHandler);
			fileStream.openAsync(cacheTableFile, FileMode.WRITE);
			cacheTable = new Dictionary();
			fileStream.writeObject(cacheTable);
			fileStream.close();

			function closeHandler(e:Event):void
			{
				fileStream.removeEventListener(Event.CLOSE, closeHandler);
				fileStream.removeEventListener(IOErrorEvent.IO_ERROR, errorHandler);
				dispatchEvent(new CacheEvent(CacheEvent.CACHETABLE_CREATE));
				incrementCreatedCounter();
			}
			function errorHandler(e:IOErrorEvent):void
			{
				fileStream.removeEventListener(Event.CLOSE, closeHandler);
				fileStream.removeEventListener(IOErrorEvent.IO_ERROR, errorHandler);
				dispatchEvent(new CacheEvent(CacheEvent.CACHETABLE_ERROR,'creating cache table error'));
			}
		}
		
		private function createCacheDirectory():void
		{
			try
			{
				if (pathToCache.exists)
				{
					pathToCache.deleteDirectoryAsync(true);
					pathToCache.addEventListener(Event.COMPLETE, removingCompleteHandler);
				}
				pathToCache.createDirectory();
				incrementCreatedCounter();
			}
			catch (e:Error)
			{
				dispatchEvent(new CacheEvent(CacheEvent.CACHE_ERROR,'creating cache directory error'));
			}
			function removingCompleteHandler(e:Event):void
			{
				pathToCache.createDirectory();
				incrementCreatedCounter();
			}
			
		}
		
		private function loadCacheTable():void
		{
			if (!pathToCache.exists || !pathToCache.isDirectory)
			{
				dispatchEvent(new CacheEvent(CacheEvent.CACHE_NOT_EXISTS)); 
				return;
			}
			if (!cacheTableFile.exists || cacheTableFile.size == 0)
			{
				dispatchEvent(new CacheEvent(CacheEvent.CACHETABLE_ERROR, 'cache table not exists'));
				dispatchEvent(new CacheEvent(CacheEvent.CACHE_NOT_EXISTS));
				return;
			}
			var cacheStream:FileStream = new FileStream();
			cacheStream.addEventListener(Event.COMPLETE, completeHandler);
			cacheStream.addEventListener(IOErrorEvent.IO_ERROR, errorHandler);
			cacheStream.openAsync(cacheTableFile, FileMode.READ);
			function completeHandler(e:Event):void
			{
				cacheTable = cacheStream.readObject();
				//cacheTable = cacheStream.readObject() as Dictionary;
				cacheStream.close();
				loaded = true;
				dispatchEvent(new CacheEvent(CacheEvent.LOADED));
				dispatchEvent(new Event(Event.COMPLETE));
				cacheStream.removeEventListener(Event.COMPLETE, completeHandler);
				cacheStream.removeEventListener(IOErrorEvent.IO_ERROR, errorHandler);
			}
			function errorHandler(e:IOErrorEvent):void
			{
				cacheStream.close();
				cacheStream.removeEventListener(Event.COMPLETE, completeHandler);
				cacheStream.removeEventListener(IOErrorEvent.IO_ERROR, errorHandler);
				dispatchEvent(new CacheEvent(CacheEvent.CACHETABLE_ERROR, 'loading cache table error'));
			}
		}
		private function incrementCreatedCounter():void
		{
			createdCounter++;
			if (createdCounter == 2)
			{
				dispatchEvent(new CacheEvent(CacheEvent.CREATED));
				dispatchEvent(new Event(Event.COMPLETE));
			}
		}	
	}

}