package ru.segrys.caches 
{
	import flash.events.EventDispatcher;
	import flash.utils.ByteArray;
	/**
	 * Класс описывает абстрактный кэш с минимально допустимым набором методов.
	 * Нельзя создать объект этого класса, методы должны быть перегружены в наследниках
	 * @see ru.segrys.caches.ICache
	 * @author Novikov
	 */
	public class AbstractCache extends EventDispatcher implements ICache 
	{
		/**
		 * В поле data помещаются данные после их загрузки командой load
		 */
		public var data:ByteArray;
		/**
		 * Сюда помещается ключ после загрузки/выгрузки данных
		 */
		
		 /**
		  * Загружен ли кэш?
		  */
		protected var _loaded:Boolean = false;
		 
		public var key:Object; 
		/**
		 * Метод load осуществляет загрузку данных из кэша по ключу key
		 * @param	key значение ключа, по которому находятся кэшированные данные
		 */
		 
		public function load(key:Object):void
		{
			throw new Error('method must be overridden');
		}
		/**
		 * Метод saveItem осуществляет сохранение данных в кэш по ключу
		 * @param	item сохраняемые данные
		 * @param	key ключ
		 */
		public function saveItem(item:Object,key:Object):void
		{
			throw new Error('method must be overridden');
		}
		/**
		 * hasItem отвечает на вопрос - существуют ли в кэша данные по заданному ключу.
		 * Возвращает идентификатор на кэшированные данные, в случае их отстствия значение будет равно null
		 * @param	key ключ
		 * @return идентификатор данных в кэше
		 */
		public function hasItem(key:Object):Object
		{
			throw new Error('method must be overridden');
		}
		/**
		 * Метод createCache создает новый кэш
		 */
		public function createCache():void
		{
			throw new Error('method must be overridden');
		}
		/**
		 * Метод loadCache загружает существующий кэш
		 */
		public function loadCache():void
		{
			throw new Error('method must be overridden');
		}
		/**
		 * Метод clearCache осуществляет очистку кэша
		 */
		public function clearCache():void
		{
			throw new Error('method must be overridden');
		}
		/**
		 * Метод updateCache осуществляет обновление кэша (это касается кэш таблицы). Следует вызывать по окончанию работы с кэшем
		 */
		public function updateCache():void
		{
			throw new Error('method must be overridden');
		}
		
		public function set loaded(val:Boolean):void
		{
			_loaded = val;
		}
		
		public function get loaded():Boolean
		{
			return _loaded;
		}
	}
}
