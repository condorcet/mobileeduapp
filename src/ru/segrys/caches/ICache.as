package ru.segrys.caches 
{
	
	/**
	 * ...
	 * @author Novikov
	 */
	public interface ICache 
	{
		function load(key:Object):void; //загрузить данные из кэша по ключу key
		function saveItem(item:Object,key:Object):void; //сохранить данные в кэш
		function hasItem(key:Object):Object; //существуют ли данные в кэше по запрашиваемому ключу
		function createCache():void;
		function loadCache():void;
		function clearCache():void;
		function updateCache():void;
	}
	
}