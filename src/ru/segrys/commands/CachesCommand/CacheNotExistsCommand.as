package ru.segrys.commands.CachesCommand 
{
	import robotlegs.bender.bundles.mvcs.Command;
	import ru.segrys.application.Caches.ApplicationCacheFacade;
	import ru.segrys.events.Application.Cache.CacheFacadeErrorEvent;
	
	/**
	 * Действия в случае отсутствия кэша
	 * @author Novikov
	 */
	public class CacheNotExistsCommand extends Command 
	{
		[Inject]
		public var cacheFacade:ApplicationCacheFacade
		
		[Inject]
		public var event:CacheFacadeErrorEvent;
		
		public function CacheNotExistsCommand() 
		{
			super();
			
		}
		
		override public function execute():void
		{
			trace('кэш отстуствует, создаем...');
			cacheFacade.createCache(event.cache);
		}
	}

}