package ru.segrys.commands.UnitsCommand 
{
	import robotlegs.bender.bundles.mvcs.Command;
	import ru.segrys.actors.DataActors.ListOfModelsActor;
	import ru.segrys.actors.DataActors.ModelActor;
	import ru.segrys.Do.ListItem.ModelsItemDataObject;
	import ru.segrys.Do.ListOfModelsDataObject;
	import ru.segrys.events.ActorEvent.DataActors.UnitsEvent;
	
	/**
	 * ...
	 * @author Novikov
	 */
	public class LoadUnitModelCommand extends Command 
	{
		
		[Inject]
		public var event:UnitsEvent;
		
		[Inject]
		public var modelActor:ModelActor;
		
		[Inject]
		public var listOfModelsActor:ListOfModelsActor;
		
		public function LoadUnitModelCommand() 
		{
			super();
			
		}
		
		override public function execute():void
		{
			//если url не указан, то загрузка первой модели в списке модели данных listOfModelsActor
			if (!event.url)
			{
				modelActor.modelURL = (listOfModelsActor.list.getItemAt(0) as ModelsItemDataObject).modelURL;
			}
			else
				modelActor.modelURL = event.url;
			//modelActor.loadModel(); РАСКОММЕНТИТЬ !!!
		}
		
	}

}