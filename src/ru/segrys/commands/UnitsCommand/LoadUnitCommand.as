package ru.segrys.commands.UnitsCommand 
{
	import flash.events.Event;
	import flash.events.IEventDispatcher;
	import flash.net.URLRequest;
	import robotlegs.bender.bundles.mvcs.Command;
	import ru.segrys.actors.DataActors.ChapterActor;
	import ru.segrys.actors.DataActors.UnitActor;
	import ru.segrys.application.Caches.ApplicationCacheFacade;
	import ru.segrys.application.Loaders.Adapters.XMLLoaderAdapter;
	import ru.segrys.application.Loaders.ApplicationLoadersFacade;
	import ru.segrys.Do.UnitActorDataObject;
	import ru.segrys.events.ActorEvent.DataActors.UnitsEvent;
	import ru.segrys.events.loader.LoaderEvent
	import ru.segrys.events.parser.ParserEvent;
	import ru.segrys.parser.UnitParser;

	/**
	 * ...
	 * @author Novikov
	 */
	public class LoadUnitCommand extends Command
	{
		[Inject]
		public var loaderFacade:ApplicationLoadersFacade;
		
		[Inject]
		public var cacheFacade:ApplicationCacheFacade;
		
		[Inject]
		public var chapterActor:ChapterActor;
		
		[Inject]
		public var unitActor:UnitActor;
		
		[Inject]
		public var event:UnitsEvent;
		
		[Inject]
		public var eventDispatcher:IEventDispatcher;
		
		public function LoadUnitCommand() 
		{
			
		}
		override public function execute():void
		{
			var unitLoader:XMLLoaderAdapter = new XMLLoaderAdapter(loaderFacade, cacheFacade);
			unitLoader.addEventListener(LoaderEvent.COMPLETE, unitDataLoaded);
			unitLoader.addEventListener(LoaderEvent.ERROR, unitDataError);
			unitLoader.load(new URLRequest(event.url));
			var unitParser:UnitParser = new UnitParser();
			
			function unitDataLoaded(e:LoaderEvent):void
			{
				unitParser.addEventListener(ParserEvent.COMPLETE, parseComplete);
				unitParser.data = unitLoader.data;
				unitParser.parse();
				unitLoader.destroy();
			}
			
			function parseComplete(e:ParserEvent):void
			{
				unitActor.setData(unitParser.result as UnitActorDataObject);
				trace('header: '+(unitActor.getData() as UnitActorDataObject).header);
				unitActor.chapterURL = chapterActor.chapterURL;
				unitActor.unitURL = event.url;
				eventDispatcher.dispatchEvent(new UnitsEvent(UnitsEvent.LOAD_UNIT_MODEL));
			}
			
			function unitDataError(e:LoaderEvent):void
			{
				unitLoader.destroy();
			}
			
		}
		
		
	}

}