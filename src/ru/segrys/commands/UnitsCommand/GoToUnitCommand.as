package ru.segrys.commands.UnitsCommand 
{
	import flash.events.IEventDispatcher;
	import robotlegs.bender.bundles.mvcs.Command;
	import ru.segrys.events.ActorEvent.DataActors.UnitsEvent;
	
	/**
	 * ...
	 * @author Novikov
	 */
	public class GoToUnitCommand extends Command 
	{
		
		[Inject]
		public var event:UnitsEvent;
		
		[Inject]
		public var eventDispatcher:IEventDispatcher;
		
		public function GoToUnitCommand() 
		{
			super();
			
		}
		
		override public function execute():void
		{
			eventDispatcher.dispatchEvent(new UnitsEvent(UnitsEvent.LOAD_UNIT, event.url));
		}
		
	}

}