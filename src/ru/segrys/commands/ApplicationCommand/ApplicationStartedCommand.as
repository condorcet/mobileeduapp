package ru.segrys.commands.ApplicationCommand 
{
	import robotlegs.bender.bundles.mvcs.Command;
	import ru.segrys.application.Caches.ApplicationCacheFacade;
	import ru.segrys.application.Loaders.Adapters.XMLLoaderAdapter;
	import ru.segrys.application.Loaders.Adapters.ItemLoaderAdapter;
	import ru.segrys.application.Loaders.ApplicationLoadersFacade;
	import flash.net.URLRequest;
	import flash.events.Event;
	import ru.segrys.events.loader.LoaderEvent;
	
	/**
	 * Приложение запущено
	 * @author Novikov
	 */
	public class ApplicationStartedCommand extends Command 
	{
		
		[Inject]
		public var cacheFacade:ApplicationCacheFacade;
		
		[Inject]
		public var loaderFacade:ApplicationLoadersFacade;
		
		public function ApplicationStartedCommand() 
		{
			
		}
		override public function execute():void
		{
			trace('app started');			
		}
	}

}