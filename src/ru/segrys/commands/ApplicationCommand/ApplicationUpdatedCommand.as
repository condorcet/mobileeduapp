package ru.segrys.commands.ApplicationCommand 
{
	import robotlegs.bender.bundles.mvcs.Command;
	
	/**
	 * Приложение обновлено
	 * @author Novikov
	 */
	//Команда вызывающаяся по результатам успешного обновления приложения
	public class ApplicationUpdatedCommand extends Command 
	{
		
		public function ApplicationUpdatedCommand() 
		{
			
		}
		
	}

}