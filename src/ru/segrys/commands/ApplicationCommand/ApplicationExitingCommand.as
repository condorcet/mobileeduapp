package ru.segrys.commands.ApplicationCommand 
{
	import robotlegs.bender.bundles.mvcs.Command;
	
	/**
	 * Процесс выхода из приложения
	 * @author Novikov
	 */
	public class ApplicationExitingCommand extends Command 
	{
		
		public function ApplicationExiting() 
		{
			
		}
		override public function execute():void
		{
			/*
			 * Обновить кэш-таблицы.
			 * ...
			 * */
		}
	}

}