package ru.segrys.commands.ApplicationCommand 
{
	import robotlegs.bender.bundles.mvcs.Command;
	
	/**
	 * Команда перехода приложения в онлайн режим работы
	 * @author Novikov
	 */
	public class GoToOnlineModeCommand extends Command 
	{
		
		public function GoToOnlineModeCommand() 
		{
			
		}
		override public function execute():void
		{
			/*Перевод параметров загрузчиков ресурсов в онлайн режим
			 * */
		}
	}

}