package ru.segrys.commands.ApplicationCommand 
{
	import robotlegs.bender.bundles.mvcs.Command;
	
	/**
	 * Команда перехода приложения в автономный режим работы
	 * @author Novikov
	 */
	public class GoToOfflineModeCommand extends Command 
	{
		
		public function GoToOfflineModeCommand() 
		{
			
		}
		override public function execute():void
		{
			/*Перевод параметров загрузчиков ресурсов в автономный режим
			 * */
		}
	}

}