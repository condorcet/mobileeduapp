package ru.segrys.commands.ApplicationCommand 
{
	import robotlegs.bender.bundles.mvcs.Command;
	
	/**
	 * Успешный выход из приложения
	 * @author Novikov
	 */
	public class ApplicationExitSuccessfullCommand extends Command 
	{
		
		public function ApplicationExitSuccessfull() 
		{
			
		}
		override public function execute():void
		{
			
		}
	}

}