package ru.segrys.commands.ApplicationCommand 
{
	import robotlegs.bender.bundles.mvcs.Command;
	
	/**
	 * Команда о неуспешном обновлении приложения
	 * @author Novikov
	 */
	public class ApplicationUpdateFailCommand extends Command 
	{
		
		public function ApplicationUpdateFailCommand() 
		{
			
		}
		override public function execute():void
		{
			/*Вывод сообщения о событии пользователю
			 * */
		}
	}

}