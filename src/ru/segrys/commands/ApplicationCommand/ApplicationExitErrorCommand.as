package ru.segrys.commands.ApplicationCommand 
{
	import robotlegs.bender.bundles.mvcs.Command;
	
	/**
	 * Ошибка выхода из приложения
	 * @author Novikov
	 */
	public class ApplicationExitErrorCommand extends Command 
	{
		
		public function ApplicationExitError() 
		{
			
		}
		override public function execute():void
		{
			/*Вывести сообщение об ошибке.
Предложить пользователю выбрать дальнейшие действия (по возможности).
*/
		}
	}

}