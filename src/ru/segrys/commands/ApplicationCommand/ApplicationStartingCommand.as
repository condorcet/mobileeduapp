package ru.segrys.commands.ApplicationCommand 
{
	import flash.events.IEventDispatcher;
	import robotlegs.bender.bundles.mvcs.Command;
	import ru.segrys.application.Caches.ApplicationCacheFacade;
	import ru.segrys.application.Caches.Fabrics.ICacheFabric;
	import ru.segrys.application.Loaders.ApplicationLoadersFacade;
	import ru.segrys.caches.AbstractCache;
	import ru.segrys.caches.ICache;
	import ru.segrys.params.AppParamsLoader;
	import ru.segrys.params.AppParams;
	import ru.segrys.events.Application.Params.ParamsLoaderEvent;
	import ru.segrys.events.Application.ApplicationEvent;
	import ru.segrys.application.Caches.CacheFabrics;
	import ru.segrys.events.cache.CacheEvent;
	/**
	 * Приложение запускается
	 * @author Novikov
	 */
	public class ApplicationStartingCommand extends Command 
	{
		
		[Inject]
		public var dispatcher:IEventDispatcher;
		
		[Inject]
		public var cacheFacade:ApplicationCacheFacade;
		
		[Inject]
		public var loadersFacade:ApplicationLoadersFacade;
		
		/**
		 * Счетчик инициализрованных (загруженных/созданных) кэшей
		 */
		private var initedCaches:uint = 0;
		
		private var currentCache:AbstractCache;
		
		private var listOfCaches:Array;
		
		public function ApplicationStartingCommand() 
		{
			
		}
		override public function execute():void
		{
			/*
			 * Инициализация параметров приложения. */
			var paramsLoader:AppParamsLoader = AppParamsLoader.getInstance();
			paramsLoader.addEventListener(ParamsLoaderEvent.PARAMS_LOADED, paramsLoaded);
			paramsLoader.addEventListener(ParamsLoaderEvent.PARAMS_LOAD_ERROR, paramsLoaderError);
			paramsLoader.loadData();
			
			function paramsLoaderError(e:ParamsLoaderEvent):void
			{
				//ошибка чтения файла с параметрами; ошибка передачи загруженных данных в AppParams
				dispatcher.dispatchEvent(new ApplicationEvent(ApplicationEvent.APPLICATION_STARTING_ERROR));
				//восстановить файл с настройками и загрузить заново
			}
			
			function paramsLoaded(e:ParamsLoaderEvent):void
			{
				/*
				Загрузка кэша (при необходимости)
				Загрузка главной страницы */
				loadersFacade.cachable = AppParams.cachable;
				dispatcher.dispatchEvent(new ApplicationEvent(ApplicationEvent.APPLICATION_STARTED));
				/*if (loadersFacade.cachable)
				{
					listOfCaches = cacheFacade.getCaches();
					initCaches();				
				}*/
			}
			
			/** Временное решение проблемы загрузки при отсутствии кэшей
			 *
			 **/
			function initCaches():void
			{
				if (initedCaches < listOfCaches.length)
				{
					currentCache = listOfCaches[initedCaches];
					currentCache.addEventListener(CacheEvent.LOADED, cacheLoaded);
					currentCache.addEventListener(CacheEvent.CACHE_NOT_EXISTS, cacheError);
				}
				else
				{
					dispatcher.dispatchEvent(new ApplicationEvent(ApplicationEvent.APPLICATION_STARTED));
				}
				function cacheLoaded(e:CacheEvent):void
				{
					initedCaches++;
					currentCache.removeEventListener(CacheEvent.LOADED, cacheLoaded);
					currentCache.addEventListener(CacheEvent.CACHE_NOT_EXISTS, cacheError);
				}
				function cacheError(e:CacheEvent):void
				{
					currentCache.createCache();
					currentCache.addEventListener(CacheEvent.CREATED, cacheCreated);
					currentCache.removeEventListener(CacheEvent.CACHE_NOT_EXISTS, cacheError);
				}
				function cacheCreated(e:CacheEvent):void
				{
					currentCache.removeEventListener(CacheEvent.CREATED, cacheCreated);
					currentCache.loadCache();
				}
			}
			
			
		}
	}

}