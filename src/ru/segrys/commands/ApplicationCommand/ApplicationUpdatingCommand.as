package ru.segrys.commands.ApplicationCommand 
{
	import robotlegs.bender.bundles.mvcs.Command;
	
	/**
	 * Команда о процессе обновления приложения
	 * @author Novikov
	 */
	public class ApplicationUpdatingCommand extends Command 
	{
		
		public function ApplicationUpdatingCommand() 
		{
			
		}
		override public function execute():void
		{
			/*Вывести окно загрузки о прогрессе скачивания/установки приложения.
По результатам скачивания/установки инициировать события «Приложение обновлено» или «Приложение не обновлено»
*/
		}
	}

}