package ru.segrys.commands.ChaptersCommand 
{
	import flash.net.URLRequest;
	import robotlegs.bender.bundles.mvcs.Command;
	import ru.segrys.actors.DataActors.ChapterActor;
	import ru.segrys.actors.DataActors.ListOfUnitsActor;
	import ru.segrys.application.Caches.ApplicationCacheFacade;
	import ru.segrys.application.Loaders.Adapters.XMLLoaderAdapter;
	import ru.segrys.application.Loaders.ApplicationLoadersFacade;
	import ru.segrys.Do.ChapterDataObject;
	import ru.segrys.events.ActorEvent.DataActors.ChaptersEvent;
	import ru.segrys.events.loader.LoaderEvent;
	import ru.segrys.events.parser.ParserEvent;
	import ru.segrys.parser.ChapterParser;
	
	/**
	 * ...
	 * @author Novikov
	 */
	public class LoadChapterCommand extends Command 
	{
		[Inject]
		public var event:ChaptersEvent;
		
		[Inject]
		public var loaderFacade:ApplicationLoadersFacade;
		
		[Inject]
		public var cacheFacade:ApplicationCacheFacade;
		
		[Inject]
		public var chapterModel:ChapterActor;
		
		[Inject]
		public var listOfUnitsModel:ListOfUnitsActor;
		
		public function LoadChapterCommand() 
		{
			super();
			
		}
		
		override public function execute():void
		{
			var loader:XMLLoaderAdapter = new XMLLoaderAdapter(loaderFacade, cacheFacade);
			var parser:ChapterParser = new ChapterParser();
			loader.addEventListener(LoaderEvent.COMPLETE, loaderCompleteHandler);
			loader.addEventListener(LoaderEvent.ERROR, loaderErrorHandler);
			loader.load(new URLRequest(event.url));
			
			
			function loaderCompleteHandler(e:LoaderEvent):void
			{
				parser.data = loader.data as XML;
				parser.addEventListener(ParserEvent.COMPLETE, parserCompleteHandler);
				parser.parse();
				
			}
			
			function loaderErrorHandler(e:LoaderEvent):void
			{
				trace('Ошибка при загрузке списка дидактических единиц раздела '+event.url);
			}
			
			function parserCompleteHandler(e:ParserEvent):void
			{
				chapterModel.setData(parser.result);
				listOfUnitsModel.setData((parser.result as ChapterDataObject).listOfUnits);
				chapterModel.chapterURL = event.url;
			}
			
			trace('go to chapter command inited: '+event.url);
		}
		
	}

}