package ru.segrys.commands.ChaptersCommand 
{
	import flash.net.URLLoader;
	import robotlegs.bender.bundles.mvcs.Command;
	import ru.segrys.actors.DataActors.ListOfChaptersActor;
	import ru.segrys.application.Caches.ApplicationCacheFacade;
	import ru.segrys.application.Loaders.Adapters.XMLLoaderAdapter;
	import ru.segrys.application.Loaders.Adapters.ItemLoaderAdapter;
	import ru.segrys.application.Loaders.ApplicationLoadersFacade;
	import flash.net.URLRequest;
	import flash.events.Event;
	import ru.segrys.Do.DataListActorObject;
	import ru.segrys.Do.ListOfChaptersDataObject;
	import ru.segrys.events.loader.LoaderEvent;
	import flash.system.Security;
	import ru.segrys.events.parser.ParserEvent;
	import ru.segrys.params.AppParams;
	import ru.segrys.parser.ListOfChaptersParser;
	/**
	 * ...
	 * @author Novikov
	 */
	public class LoadChaptersCommand extends Command 
	{
		[Inject]
		public var cacheFacade:ApplicationCacheFacade;
		
		[Inject]
		public var loaderFacade:ApplicationLoadersFacade;
		
		[Inject]
		public var chaptersActor:ListOfChaptersActor;
		
		public function LoadChaptersCommand() 
		{
			super();
		}
		
		override public function execute():void
		{
			var loader:XMLLoaderAdapter = new XMLLoaderAdapter(loaderFacade, cacheFacade);
			var req:URLRequest = new URLRequest(AppParams.chaptersURL);
			loader.addEventListener(LoaderEvent.COMPLETE, completeHandler);
			loader.load(req);
			function completeHandler(e:Event):void
			{
				loader.removeEventListener(LoaderEvent.COMPLETE, completeHandler);
				var result:XML = loader.data as XML;
				trace('complete: '+(loader.data as XML));
				loader.destroy();
				var chaptersParser:ListOfChaptersParser = new ListOfChaptersParser();
				chaptersParser.data = result;
				chaptersParser.addEventListener(ParserEvent.COMPLETE, parseComplete);
				chaptersParser.parse();
				function parseComplete(e:ParserEvent):void
				{
					chaptersParser.removeEventListener(ParserEvent.COMPLETE, parseComplete);
					chaptersActor.setData(chaptersParser.result);
				}
			}
		}
		
	}

}