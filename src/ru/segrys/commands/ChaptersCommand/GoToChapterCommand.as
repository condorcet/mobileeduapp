package ru.segrys.commands.ChaptersCommand 
{
	import flash.events.IEventDispatcher;
	import robotlegs.bender.bundles.mvcs.Command;
	import ru.segrys.events.ActorEvent.DataActors.ChaptersEvent;
	
	/**
	 * ...
	 * @author Novikov
	 */
	public class GoToChapterCommand extends Command 
	{
		[Inject]
		public var eventDispatcher:IEventDispatcher;
		
		[Inject]
		public var event:ChaptersEvent;
		
		public function GoToChapterCommand() 
		{
			super();
			
		}
		
		override public function execute():void
		{
			eventDispatcher.dispatchEvent(new ChaptersEvent(ChaptersEvent.LOAD_CHAPTER, event.url));
		}
	}

}