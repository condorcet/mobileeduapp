package ru.segrys.events.Application.Cache 
{
	import flash.events.Event;
	import ru.segrys.application.Caches.Fabrics.ICacheFabric;
	
	/**
	 * Событие ошибки кэша. В параметре cache передаается ссылка на класс фабрику кэша, в котором произошла ошибка
	 * @author Novikov
	 */
	public class CacheFacadeErrorEvent extends Event 
	{
		
		public var cache:ICacheFabric;
		
		public function CacheFacadeErrorEvent(type:String, cache:ICacheFabric = null, bubbles:Boolean=false, cancelable:Boolean=false) 
		{ 
			super(type, bubbles, cancelable);
			this.cache = cache;
		} 
		
		public override function clone():Event 
		{ 
			return new CacheFacadeErrorEvent(type, cache, bubbles, cancelable);
		} 
		
		public override function toString():String 
		{ 
			return formatToString("CacheFacadeErrorEvent", "type", "bubbles", "cancelable", "eventPhase"); 
		}
		/**
		 * Директория кэша и/или кэш-таблица отсутствуют
		 */
		public static const CACHE_NOT_EXISTS:String = 'cacheFacade_cacheNotExists';
		
	}
	
}