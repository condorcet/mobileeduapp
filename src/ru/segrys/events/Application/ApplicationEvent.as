package ru.segrys.events.Application 
{
	import flash.events.Event;
	
	/**
	 * Событие приложения
	 * @author Novikov
	 */
	public class ApplicationEvent extends Event 
	{
		
		public function ApplicationEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false) 
		{ 
			super(type, bubbles, cancelable);
			
		} 
		
		public override function clone():Event 
		{ 
			return new ApplicationEvent(type, bubbles, cancelable);
		} 
		
		public override function toString():String 
		{ 
			return formatToString("ApplicationEvent", "type", "bubbles", "cancelable", "eventPhase"); 
		}
		/**
		 * Событие начала процесса запуска приложения
		 */
		public static const APPLICATION_STARTING:String = 'applicationStarting';
		/**
		 * Событие окончания запуска приложения
		 */
		public static const APPLICATION_STARTED:String = 'applicationStarted';
		/**
		 * Событие ошибки запуска приложения
		 */
		public static const APPLICATION_STARTING_ERROR:String = 'applicationStartingError';
		/**
		 * Событие процесса завершения приложения
		 */
		public static const APPLICATION_EXITING:String = 'applicationExiting';
		/**
		 * Событие успешного завершения приложения
		 */
		public static const APPLICATION_EXIT_SUCCESSFULL:String = 'applicationExitSuccessfull';
		/**
		 * Событие неуспешного завершения приложения
		 */
		public static const APPLICATION_EXIT_ERROR:String = 'applicationExitError';
		/**
		 * Событие запроса на обновление приложения
		 */
		public static const UPDATE_APPLICATION:String = 'updateApplication';
		/**
		 * Событие успешного обновления приложения
		 */
		public static const APPLICATION_UPDATED:String = 'applicationUpdated';
		/**
		 * Событие неуспешного обновления приложения
		 */
		public static const APPLICATION_UPDATE_FAIL:String = 'applicationUpdateFail';
		/**
		 * Событие процесса обновления приложения
		 */
		public static const APPLICATION_UPDATING:String = 'applicationUpdating';
		/**
		 * Событие переключения приложения в автономный режим
		 */
		public static const APPLICATION_OFFLINE_MODE:String = 'applicationOfflineMode';
		/**
		 * Событие переключения приложения в онлайн режим
		 */
		public static const APPLICATION_ONLINE_MODE:String = 'applicationOnlineMode';
		/**
		 * Событие очистки кэшей приложения
		 */
		public static const APPLICATION_CLEAR_CACHE:String = 'applicationClearCache';
	}
	
}