package ru.segrys.events.Application.Loader 
{
	import flash.events.Event;
	
	/**
	 * Событие связанное с классом-фасадом управления загрузчиками
	 * @see ru.segrys.Loaders.ApplicationLoadersFacade
	 * @author Novikov
	 */
	public class ApplicationLoaderFacadeEvent extends Event 
	{
		
		public function ApplicationLoaderFacadeEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false) 
		{ 
			super(type, bubbles, cancelable);
			
		} 
		
		public override function clone():Event 
		{ 
			return new ApplicationLoaderFacadeEvent(type, bubbles, cancelable);
		} 
		
		public override function toString():String 
		{ 
			return formatToString("ApplicationLoaderFacadeEvent", "type", "bubbles", "cancelable", "eventPhase"); 
		}
		/**
		 * Кэширование данных активировано
		 * @eventType cachingEnable
		 */
		public static const CACHING_ENABLED:String = 'cachingEnable';
		/**
		 * Кэширование данных отключено
		 */
		public static const CACHING_DISABLED:String = 'cachingDisable'
	}
	
}