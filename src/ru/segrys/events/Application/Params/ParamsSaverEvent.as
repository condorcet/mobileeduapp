package ru.segrys.events.Application.Params 
{
	import flash.events.Event;
	
	/**
	 * Событие сохранения параметров приложения
	 * @see ru.segrys.params.AppParamSaver
	 * @author Novikov
	 */
	public class ParamsSaverEvent extends Event 
	{
		
		public function ParamsSaverEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false) 
		{ 
			super(type, bubbles, cancelable);
			
		} 
		
		public override function clone():Event 
		{ 
			return new ParamsSaverEvent(type, bubbles, cancelable);
		} 
		
		public override function toString():String 
		{ 
			return formatToString("ParamsSaverEvent", "type", "bubbles", "cancelable", "eventPhase"); 
		}
		/**
		 * Параметры приложения сохранены
		 */
		public static const PARAMS_SAVED:String = 'paramsSaved';
		/**
		 * Ошибка сохранения параметров приложения
		 */
		public static const PARAMS_SAVE_ERROR:String = 'paramsSaveError';
	}
	
}