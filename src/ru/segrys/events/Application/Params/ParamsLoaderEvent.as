package ru.segrys.events.Application.Params 
{
	import flash.events.Event;
	
	/**
	 * Событие загрузки параметров приложения
	 * @see ru.segrys.params.AppParamLoader
	 * @author Novikov
	 */
	public class ParamsLoaderEvent extends Event 
	{
		
		public function ParamsLoaderEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false) 
		{ 
			super(type, bubbles, cancelable);
			
		} 
		
		public override function clone():Event 
		{ 
			return new ParamsLoaderEvent(type, bubbles, cancelable);
		} 
		
		public override function toString():String 
		{ 
			return formatToString("ParamsLoaderEvent", "type", "bubbles", "cancelable", "eventPhase"); 
		}
		/**
		 * Параметры приложения загружены
		 */
		public static const PARAMS_LOADED:String = 'paramsLoaded';
		/**
		 * Ошибка загрузки параметров приложения
		 */
		public static const PARAMS_LOAD_ERROR:String = 'paramsLoadedFail';
		
	}
	
}