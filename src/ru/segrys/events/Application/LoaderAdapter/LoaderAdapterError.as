package ru.segrys.events.Application.LoaderAdapter 
{
	import flash.events.Event;
	import ru.segrys.application.Loaders.Adapters.ItemLoaderAdapter;
	/**
	 * Ошибка адапетра загрузчика
	 * @see ru.segrys.application.Loaders.Adapters.ItemLoaderAdapter
	 * @author Novikov
	 */
	public class LoaderAdapterError extends Event 
	{
		private var loaderAdapter:ItemLoaderAdapter;
		public function LoaderAdapterError(type:String, loader:ItemLoaderAdapter, bubbles:Boolean=false, cancelable:Boolean=false) 
		{ 
			loaderAdapter = loader;
			super(type, bubbles, cancelable);
		} 
		
		public override function clone():Event 
		{ 
			return new LoaderAdapterError(type, loaderAdapter, bubbles, cancelable);
		} 
		
		public override function toString():String 
		{ 
			return formatToString("LoaderAdapterError", "type", "bubbles", "cancelable", "eventPhase"); 
		}
		/**
		 * Ошибка загрузки кэш-таблицы
		 */
		public static const CACHE_TABLE:String = 'cacheTableError';
		/**
		 * Ошибка сохранения данных в кэш
		 */
		public static const ITEM_SAVE:String = 'itemSaveError';
		/**
		 * Ошибка кэша
		 */
		public static const CACHE:String = 'cacheError';
	}
	
}