package ru.segrys.events.loader 
{
	import flash.events.Event;
	
	/**
	 * Событие загрузчика
	 * @author Novikov
	 */
	public class LoaderEvent extends Event 
	{
		
		public function LoaderEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false) 
		{ 
			super(type, bubbles, cancelable);
		} 
		
		public override function clone():Event 
		{ 
			return new LoaderEvent(type, bubbles, cancelable);
		} 
		
		public override function toString():String 
		{ 
			return formatToString("LoaderEvent", "type", "bubbles", "cancelable", "eventPhase"); 
		}
		/**
		 * Загрузка завершена
		 */
		public static const COMPLETE:String = 'loadingComplete';
		/**
		 * Ошибка загрузки
		 */
		public static const ERROR:String = 'loadingError';
		/**
		 * Данные сериализованы
		 */
		public static const SERIALIZED:String = 'dataSerialized';
	}
	
}