package ru.segrys.events.cache 
{
	import flash.events.Event;
	
	/**
	 * ...
	 * @author Novikov
	 */
	public class FSCacheEvent extends CacheEvent 
	{
		public function FSCacheEvent(type:String, msg:String=null, bubbles:Boolean=false, cancelable:Boolean=false) 
		{ 
			super(type, msg, bubbles, cancelable);
		} 
		
		public override function clone():Event 
		{ 
			return new FSCacheEvent(type, msg, bubbles, cancelable);
		} 
		
		public override function toString():String 
		{ 
			return formatToString("FSCacheEvent", "type", "bubbles", "cancelable", "eventPhase"); 
		}
		
	}
	
}