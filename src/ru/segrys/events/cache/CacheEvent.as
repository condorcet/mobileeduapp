package ru.segrys.events.cache 
{
	import flash.events.Event;
	
	/**
	 * Событие кэша
	 * @see ru.segrys.caches.AbstractCache
	 * @author Novikov
	 */
	public class CacheEvent extends Event 
	{
		public var msg:String;
		public function CacheEvent(type:String, msg:String = null, bubbles:Boolean=false, cancelable:Boolean=false) 
		{ 
			this.msg = msg;
			super(type, bubbles, cancelable);
			
		} 
		
		public override function clone():Event 
		{ 
			return new CacheEvent(type, msg, bubbles, cancelable);
		} 
		
		public override function toString():String 
		{ 
			return formatToString("FSCacheEvent", "type", "bubbles", "cancelable", "eventPhase"); 
		}
		
		
		/** 
		* Директория кэша или кэш-таблица отстутсвуют
		*/ 
		public static const CACHE_NOT_EXISTS:String = 'cacheNotExist';
		/**
		 * Ошибка выполнения операции над кэшом
		 */
		public static const CACHE_ERROR:String = 'cacheError';
		/**
		 * Ошибка выполнения операции над кэш-таблицой
		 */
		public static const CACHETABLE_ERROR:String = 'cacheTableError';
		/**
		 * Кэш-таблица создана
		 */
		public static const CACHETABLE_CREATE:String = 'cacheTableCreate';
		/**
		 * Кэш успешно загружен
		 */
		public static const LOADED:String = 'cacheLoaded';
		/**
		 * Кэш не загружен
		 */
		public static const NOT_LOADED:String = 'cacheNotLoaded';
		/**
		 * Недостаточно места для сохранения данных в кэш
		 */
		public static const NOT_ENOUGH_SPACE:String = 'notEngoughSpace';
		/**
		 * Кэш создан
		 */
		public static const CREATED:String = 'cacheCreated'
		/**
		 * Кэш очищен
		 */
		public static const CLEARED:String = 'cacheCleared';
		
	}
	
}