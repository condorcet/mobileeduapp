package ru.segrys.events.cache 
{
	import flash.events.Event;
	
	/**
	 * Событие данных кэша
	 * @see ru.segrys.caches.AbstractCache
	 * @author Novikov
	 */
	public class ItemCacheEvent extends Event 
	{
		public var key:Object;
		public function ItemCacheEvent(type:String, key:Object = null, bubbles:Boolean=false, cancelable:Boolean=false) 
		{ 
			this.key = key;
			super(type, bubbles, cancelable);
			
		} 
		
		public override function clone():Event 
		{ 
			return new ItemCacheEvent(type, key, bubbles, cancelable);
		} 
		
		public override function toString():String 
		{ 
			return formatToString("ItemCacheEvent", "type", "bubbles", "cancelable", "eventPhase"); 
		}
		
		/**
		 * Данные получены из кэша
		 */
		public static const ITEM_LOADED:String = 'itemLoaded';
		/**
		 * Данные не получены из кэша
		 */
		public static const ITEM_NOT_LOADED:String = 'itemNotLoaded';
		/**
		 * Данные сохранены в кэш
		 */
		public static const ITEM_SAVED:String = 'itemSaved';
		/**
		 * Данные не сохранены в кэш
		 */
		public static const ITEM_NOT_SAVED:String = 'itemNotSaved';
		/**
		 * Данные по этому ключу отсутствуют в кэше
		 */
		public static const FILE_ITEM_NOT_EXISTS:String = 'fileItemNotExists';
		/**
		 * Данные отсутствуют в кэш таблице
		 */
		public static const ITEM_NOT_EXISTS:String = 'itemNotExists';
		/**
		 * Ошибка при удалении данных из кэша
		 */
		public static const ITEM_NOT_REMOVED:String = 'itemNotRemoved';
		/**
		 * Данные удалены из кэша
		 */
		public static const ITEM_REMOVED:String = 'itemRemoved';
	}
	
}