package ru.segrys.events.parser 
{
	import flash.events.Event;
	
	/**
	 * Событие парсера
	 * @author Novikov
	 */
	public class ParserEvent extends Event 
	{
		
		public function ParserEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false) 
		{ 
			super(type, bubbles, cancelable);
			
		} 
		
		public override function clone():Event 
		{ 
			return new ParserEvent(type, bubbles, cancelable);
		} 
		
		public override function toString():String 
		{ 
			return formatToString("ParserEvent", "type", "bubbles", "cancelable", "eventPhase"); 
		}
		/**
		 * Парсинг успешно выполнен
		 */
		public static const COMPLETE:String = 'parseComplete';
		/**
		 * Ошибка парсинга
		 */
		public static const ERROR:String = 'parseError';
	}

}