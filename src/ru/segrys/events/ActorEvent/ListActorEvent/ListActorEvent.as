package ru.segrys.events.ActorEvent.ListActorEvent 
{
	import flash.events.Event;
	
	/**
	 * Событие связанное со списковой моделью данных
	 * @author Novikov
	 */
	public class ListActorEvent extends Event 
	{
		
		public function ListActorEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false) 
		{ 
			super(type, bubbles, cancelable);
			
		} 
		
		public override function clone():Event 
		{ 
			return new ListActorEvent(type, bubbles, cancelable);
		} 
		
		public override function toString():String 
		{ 
			return formatToString("ListActorEvent", "type", "bubbles", "cancelable", "eventPhase"); 
		}
		/**
		 * Переход к следующему элементу
		 */
		public static const NEXT_ITEM:String = 'callNextItem';
		/**
		 * Переход к предыдущему элементу
		 */
		public static const PREV_ITEM:String = 'callPrevItem';
		/**
		 * Осуществлен переход к последнему элементу
		 */
		public static const LAST_ITEM:String = 'callLastItem';
		/**
		 * Осуществлен переход к начальному элементу
		 */
		public static const FIRST_ITEM:String = 'callFirstItem';
		/**
		 * Конец списка, переход к следующему элементу невозможен
		 */
		public static const END_OF_LIST:String = 'endOfList';
		/**
		 * Начало списка, переход к предыдущему элементу невозможен
		 */
		public static const TOP_OF_LIST:String = 'beginOfList';
	}
	
}