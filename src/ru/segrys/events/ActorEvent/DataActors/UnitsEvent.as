package ru.segrys.events.ActorEvent.DataActors 
{
	import flash.events.Event;
	
	/**
	 * Событие связанное с дидактической единицей
	 * @author Novikov
	 */
	public class UnitsEvent extends Event 
	{
		public var url:String;
		public function UnitsEvent(type:String, url:String = null, bubbles:Boolean=false, cancelable:Boolean=false) 
		{ 
			this.url = url;
			super(type, bubbles, cancelable);
			
		} 
		
		public override function clone():Event 
		{ 
			return new UnitsEvent(type, url, bubbles, cancelable);
		} 
		
		public override function toString():String 
		{ 
			return formatToString("UnitsEvent", "type", "bubbles", "cancelable", "eventPhase"); 
		}
		/**
		 * Загрузить список дидактических единиц
		 */
		public static const LOAD_UNITS:String = 'loadUnits';
		/**
		 * Список дидактических единиц загружен
		 */
		public static const UNITS_LOADED:String = 'unitsLoaded';
		/**
		 * Ошибка загрузки списка дидактических единиц
		 */
		public static const UNITS_LOADING_FAIL:String = 'unitsLoadingFail';
		/**
		 * Переход к выбранной дидактической единице
		 */
		public static const GO_TO_UNIT:String = 'goToUnit';
		/**
		 * Ошибка перехода к выбранной дидактической единице
		 */
		public static const GO_TO_UNIT_FAIL:String = 'goToUnitFail';
		/**
		 * Загрузка теории дидактической единицы
		 */
		public static const LOAD_UNIT_THEORY:String = 'loadUnitTheory';
		/**
		 * Ошибка загрузки теории выбранной дидактической единицы
		 */
		public static const LOAD_UNIT_THEORY_ERROR:String = 'loadUnitTheoryError';
		/**
		 * Загрузка модели дидактической единицы
		 */
		public static const LOAD_UNIT_MODEL:String = 'loadUnitModel';
		/**
		 * Ошибка загрузки модели дидактической единицы
		 */
		public static const LOAD_UNIT_MODEL_FAIL:String = 'loadUnitModelFail';
		/**
		 * Д.е. загружена
		 */
		public static const UNIT_LOADED:String = 'unitLoaded';
		/**
		 * Загрузить д.е.
		 */
		public static const LOAD_UNIT:String = 'loadUnit';
	}
	
}