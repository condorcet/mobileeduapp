package ru.segrys.events.ActorEvent.DataActors 
{
	import flash.events.Event;
	
	/**
	 * Событие связанное с разделом
	 * @author Novikov
	 */
	public class ChaptersEvent extends Event 
	{
		public var url:String;
		
		public function ChaptersEvent(type:String, url:String = null, bubbles:Boolean=false, cancelable:Boolean=false) 
		{ 
			this.url = url;
			super(type, bubbles, cancelable);
			
		} 
		
		public override function clone():Event 
		{ 
			return new ChaptersEvent(type, url, bubbles, cancelable);
		} 
		
		public override function toString():String 
		{ 
			return formatToString("ChaptersEvent", "type", "bubbles", "cancelable", "eventPhase"); 
		}
		/**
		 * Загрузить список разделов
		 * @eventType loadChapters
		 */
		public static const LOAD_CHAPTERS:String = 'loadChapters';
		/**
		 * Список разделов загружен
		 */
		public static const CHAPTERS_LOADED:String = 'chaptersLoaded';
		/**
		 * Ошибка загрузки списка разделов
		 */
		public static const CHAPTERS_LOADING_FAIL:String = 'chaptersLoadingFail';
		/**
		 * Переход к выбранному разделу
		 */
		public static const GO_TO_CHAPTER:String = 'goToChapter';
		/**
		 * Ошибка перехода к выбранному разделу
		 */
		public static const GO_TO_CHAPTER_FAIL:String = 'goToChapterFail';
		
		/**
		 * Загрузить раздел
		 */
		public static const LOAD_CHAPTER:String = 'loadChapter';
	}
	
}