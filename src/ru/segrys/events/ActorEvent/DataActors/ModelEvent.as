package ru.segrys.events.ActorEvent.DataActors 
{
	import flash.events.Event;
	
	/**
	 * ...
	 * @author Novikov
	 */
	public class ModelEvent extends Event 
	{
		public var url:String;
		public function ModelEvent(type:String, url:String = null, bubbles:Boolean=false, cancelable:Boolean=false) 
		{
			this.url = url;
			super(type, bubbles, cancelable);
			
		}
		
		public override function clone():Event 
		{ 
			return new UnitsEvent(type, url, bubbles, cancelable);
		}
		
		/**
		 * Загрузка модели завершена
		 */
		public static const MODEL_LOADED:String = 'modelLoaded';
		
		/**
		 * Ошибка при загрузке модели
		 */
		public static const MODEL_LOADING_ERROR:String = 'modelLoadingError';
	}

}