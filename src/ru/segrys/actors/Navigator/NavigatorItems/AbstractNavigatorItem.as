package ru.segrys.actors.Navigator.NavigatorItems 
{
	/**
	 * ...
	 * @author Novikov
	 */
	public class AbstractNavigatorItem implements INavigatorItem
	{
		/**
		 *
		 */
		public function AbstractNavigatorItem() 
		{
			
		}
		/**
		 *
		 */
		public function execute():void
		{
			throw new Error('must be overridden');
		}
	}

}