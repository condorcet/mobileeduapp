package ru.segrys.actors.Navigator.NavigatorItems 
{
	import flash.events.IEventDispatcher;
	import ru.segrys.actors.DataActors.UnitActor;
	import ru.segrys.events.ActorEvent.DataActors.UnitsEvent;
	/**
	 * ...
	 * @author ...
	 */
	public class SimpleUnitItem extends AbstractNavigatorItem 
	{
		[Inject]
		public var dispatcher:IEventDispatcher;
		
		private var url:String;
		private var header:String;
		
		public function SimpleUnitItem(url:String, header:String) 
		{
			this.url = url;
			this.header = header; 
		}
		
		override public function execute():void
		{
			//загрузка unit-а по url
			//dispatch ...
		}
		
	}

}