package  ru.segrys.actors.Navigator.NavigatorItems
{

	/**
	 * ...
	 * @author Novikov
	 */
	public class ExtendNavigatorItem implements INavigatorItem 
	{
		private var msg:String;
		public function ExtendNavigatorItem() 
		{
			msg = 'extended nav item';
		}
		public function execute():void
		{
			trace(msg);
		}
	}

}