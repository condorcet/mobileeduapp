package ru.segrys.actors.Navigator.NavigatorItems 
{
	import flash.events.IEventDispatcher;
	/**
	 * ...
	 * @author ...
	 */
	public class SimpleModelItem extends AbstractNavigatorItem 
	{
		[Inject]
		public var dispatcher:IEventDispatcher;
		
		private var url:String;
		private var header:String;
		
		public function SimpleModelItem(url:String, header:String) 
		{
			this.url = url;
			this.header = header;
			super();
		}
		
		override public function execute():void
		{
			//переход к модели по url
		}
		
	}

}