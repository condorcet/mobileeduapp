package ru.segrys.actors.Navigator.NavigatorItems 
{
	/**
	 * ...
	 * @author Novikov
	 */
	public class SampleNavigatorItem implements INavigatorItem 
	{
		
		private var msg:String;
		public function SampleNavigatorItem() 
		{
			msg = 'simple nav item';
		}
		
		public function execute():void
		{
			trace(msg);
		}
		
	}

}