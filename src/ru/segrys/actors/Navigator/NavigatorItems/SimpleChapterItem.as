package ru.segrys.actors.Navigator.NavigatorItems 
{
	import flash.events.IEventDispatcher;
	/**
	 * ...
	 * @author ...
	 */
	public class SimpleChapterItem extends AbstractNavigatorItem 
	{
		[Inject]
		public var dispatcher:IEventDispatcher;
		
		private var url:String;
		private var header:String;
		
		public function SimpleChapterItem(url:String, header:String) 
		{
			this.url = url;
			this.header = header;
			super();
		}
		
		override public function execute():void
		{
			//переход к главе по url
		}
	}

}