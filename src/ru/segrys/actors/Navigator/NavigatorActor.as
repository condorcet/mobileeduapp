package ru.segrys.actors.Navigator 
{
	import ru.segrys.actors.Navigator.NavigatorFabrics.ExtendFabric;
	import ru.segrys.actors.Navigator.NavigatorFabrics.INavigatorItemFabric;
	import ru.segrys.actors.Navigator.NavigatorFabrics.SimpleFabric;
	import ru.segrys.actors.Navigator.NavigatorItems.INavigatorItem;
	/**
	 * ...
	 * @author Novikov
	 */
	public class NavigatorActor 
	{
		
		private var list:Array; //список элементов
		private var pointer:uint; //указатель на текущий элемент
		
		public function NavigatorActor() 
		{
			list = new Array();
			pointer = 0;
		}
		
		public function push(val:String):void
		{
			//сохранить текущее состояние
			if (pointer >= 0)
			{
				while(list.length-1 >= pointer)
				{
					delete list.pop();
				}
			}
			var item:INavigatorItem = getItem(val);
			list.push(item);
			pointer++;
		}
		
		public function gotoNext():void
		{
			//перейти к след. эл-ту
			if (pointer + 1 < list.length)
			{
				pointer++;
				(list[pointer] as INavigatorItem).execute();
			}
			
		}
		
		public function gotoPrev():void
		{
			//перейти к пред. эл-ту
			if (pointer-1 >= 0)
			{
				pointer--;
				(list[pointer] as INavigatorItem).execute();
			}
			
		}
		
		public function print():void
		{
			for each(var i:INavigatorItem in list)
				trace(i);
		}
		
		private function getItem(item:String):INavigatorItem
		{
			var fabric:INavigatorItemFabric;
			switch(item)
			{
				case UNIT:
					if (pointer > 2)
						fabric = new ExtendFabric();
					else
						fabric = new SimpleFabric();
					break;
				case CHAPTER:
					break;
				case MODEL:
					break;
				default:
					trace('def');
			}
			if(fabric)
				return fabric.getItem();
			else
				return null
		}
		public static var UNIT:String = 'unitItem';
		public static var CHAPTER:String = 'chapterItem';
		public static var MODEL:String = 'modelItem';
	}
}