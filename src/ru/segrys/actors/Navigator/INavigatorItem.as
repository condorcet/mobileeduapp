package ru.segrys.actors.Navigator 
{
	
	/**
	 * ...
	 * @author Novikov
	 */
	public interface INavigatorItem 
	{
		function execute():void;
	}
	
}