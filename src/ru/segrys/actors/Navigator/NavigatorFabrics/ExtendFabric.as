package ru.segrys.actors.Navigator.NavigatorFabrics 
{
	import ru.segrys.actors.Navigator.NavigatorItems.INavigatorItem;
	import ru.segrys.actors.Navigator.NavigatorItems.ExtendNavigatorItem;
	/**
	 * ...
	 * @author ...
	 */
	public class ExtendFabric extends AbstractNavigatorItemFabric 
	{
		
		public function ExtendFabric() 
		{
			super();
		}
		
		override public function getItem():INavigatorItem
		{
			return new ExtendNavigatorItem();
		}
		
	}

}