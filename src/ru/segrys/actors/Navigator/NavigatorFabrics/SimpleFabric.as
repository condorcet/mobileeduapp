package ru.segrys.actors.Navigator.NavigatorFabrics 
{
	import ru.segrys.actors.Navigator.NavigatorItems.INavigatorItem;
	import ru.segrys.actors.Navigator.NavigatorItems.SampleNavigatorItem;
	/**
	 * ...
	 * @author ...
	 */
	public class SimpleFabric extends AbstractNavigatorItemFabric 
	{
		
		public function SimpleFabric() 
		{
			super();
		}
		override public function getItem():INavigatorItem
		{
			return new SampleNavigatorItem();
		}
		
	}

}