package ru.segrys.actors.Navigator.NavigatorFabrics 
{
	import ru.segrys.actors.DataActors.UnitActor;
	import ru.segrys.actors.Navigator.NavigatorItems.INavigatorItem;
	import ru.segrys.actors.Navigator.NavigatorItems.SimpleUnitItem;
	import ru.segrys.Do.UnitActorDataObject;
	/**
	 * ...
	 * @author ...
	 */
	public class SimpleUnitFabric extends AbstractNavigatorItemFabric 
	{
		[Inject]
		public var unit:UnitActor;
		
		public function SimpleUnitFabric() 
		{
			super();
			
		}
		
		override public function getItem():INavigatorItem
		{
			var data:UnitActorDataObject = unit.getData();
			return new SimpleUnitItem(data.unitURL, data.header);
		}
	}

}