package ru.segrys.actors.DataActors 
{
	import ru.segrys.Do.ChapterDataObject;
	import ru.segrys.Do.DataObject;
	import ru.segrys.Do.ListOfUnitsDataObject;
	/**
	 * Класс модели данных раздела
	 * @author Novikov
	 */
	public class ChapterActor extends AbstractActor 
	{
		/**
		 * URL раздела
		 */
		public var chapterURL:String; //URL раздела
		//[Inject]???
		/**
		 * Ссылка на модель данных списка дидактических единиц
		 * @see ru.segrys.Do.ListOfUnitsDataObject
		 */
		[Inject]
		public var listOfUnits:ListOfUnitsActor;
		/**
		 * Заголовок раздела
		 */
		private var header:String;
		/**
		 * Описание раздела
		 */
		private var description:String;
		
		public function ChapterActor() 
		{
			
		}
		
		override public function getData():DataObject
		{
			var tmp:ChapterDataObject = new ChapterDataObject();
			tmp.header = header;
			tmp.chapterURL = chapterURL;
			tmp.description = description;
			tmp.listOfUnits = listOfUnits.getData() as ListOfUnitsDataObject;
			return tmp;
		}
		
		override public function setData(data:DataObject):void
		{
			var tmp:ChapterDataObject = data as ChapterDataObject;
			header = tmp.header;
			chapterURL = tmp.chapterURL;
			description = tmp.description;
			listOfUnits.setData(tmp.listOfUnits);
		}
	}

}