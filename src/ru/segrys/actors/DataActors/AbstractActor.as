package ru.segrys.actors.DataActors 
{
	import flash.events.EventDispatcher;
	import flash.events.IEventDispatcher;
	import ru.segrys.Do.DataObject;
	/**
	 * Класс абстрактной модели данных.
	 * Методы должны быть переопределены в наследниках.
	 * @author Novikov
	 */
	public class AbstractActor extends EventDispatcher implements IActor 
	{
		
		[Inject]
		public var eventDispatcher:IEventDispatcher;
		
		public function AbstractActor() 
		{
		
		}
		/**
		 * Загрузить данные в модель данных 
		 * @param	data данные являются классом DataObject
		 */
		public function setData(data:DataObject):void
		{
			throw new Error('must overridden');
		}
		/**
		 * Получить данные из модели данных в виде объекта класса DataObject
		 * @return
		 */
		public function getData():DataObject
		{
			throw new Error('must overridden');
		}
	}

}