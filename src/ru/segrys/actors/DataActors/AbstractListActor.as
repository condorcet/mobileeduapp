package ru.segrys.actors.DataActors 
{
	import mx.collections.ArrayList;
	import ru.segrys.Do.DataListActorObject;
	import ru.segrys.events.ActorEvent.ListActorEvent.ListActorEvent
	import ru.segrys.Do.ListItem.ListItem;
	import ru.segrys.Do.DataObject;
	
	/**
	 * wtf?
	 * @eventType ru.segrys.events.ActorEvent.ListActorEvent.ListActorEvent.END_OF_LIST
	 */
	[Event(name = "endOfList", type = "ru.segrys.events.ActorEvent.ListActorEvent.ListActorEvent")]
	[Event(name = "lastItem", type = "ru.segrys.events.ActorEvent.ListActorEvent.ListActorEvent")]
	[Event(name = "nextItem", type = "ru.segrys.events.ActorEvent.ListActorEvent.ListActorEvent")]
	[Event(name = "topOfList", type = "ru.segrys.events.ActorEvent.ListActorEvent.ListActorEvent")]
	[Event(name = "firstItem", type = "ru.segrys.events.ActorEvent.ListActorEvent.ListActorEvent")]
	[Event(name = "prevItem", type = "ru.segrys.events.ActorEvent.ListActorEvent.ListActorEvent")]
	/**
	 * Класс абстрактной списковой модели данных.
	 * Списковая модель данных предполагает наличие данных в виде списка элементов, между которыми возможна навигация.
	 * В качестве примера можно привести модель данных списка загруженных разделов, где каждым элементом списка является ссылка на раздел
	 * @author Novikov
	 */

	public class AbstractListActor extends AbstractActor
	{
		/**
		 * Текущий элемент списка
		 */
		public var currentItem:ListItem;
		/**
		 * Список элементов
		 */
		[Bindable]
		protected var itemsList:ArrayList;
		/**
		 * Числовой указатель на текущий элемент списка
		 */
		protected var itemIndex:uint;
		
		public function AbstractListActor() 
		{
			
		}
		/**
		 * Сеттер списка
		 */
		public function set list(value:ArrayList):void
		{
			itemsList = value;
			if (itemsList.length > 0)
			{
				itemIndex = 0;
				currentItem = itemsList.getItemAt(itemIndex) as ListItem;
			}
		}
		/**
		 * Геттер списка
		 */
		public function get list():ArrayList
		{
			return itemsList;
		}
		/**
		 * Переход к следующему элементу в списке
		 * При достижении последнего эл-та списка формируется событие ListActorEvent.LAST_ITEM
		 * При переходе от последнего эл-та списка формируется событие ListActorEvent.END_OF_LIST (достигнут конец списка, переход невозможен)
		 * При успешном переходе к следующему эл-ту списка формируется событие ListActorEvent.NEXT_ITEM
		 */
		public function nextItem():void
		{
			if (itemIndex + 1 > itemsList.length)
			{
				dispatchEvent(new ListActorEvent(ListActorEvent.END_OF_LIST));
				return;
			}
			else
			if (itemIndex + 1 == itemsList.length)
				dispatchEvent(new ListActorEvent(ListActorEvent.LAST_ITEM));
			itemIndex++;
			currentItem = itemsList.getItemAt(itemIndex) as ListItem;
			dispatchEvent(new ListActorEvent(ListActorEvent.NEXT_ITEM));
		}
		/**
		 * Переход к предыдущему элементу в списке
		 * При достижении первого эл-та списка формируется событие ListActorEvent.FIRST_ITEM
		 * При переходе от первого эл-та списка формируется событие ListActorEvent.TOP_OF_LIST (достигнуто начало списка, переход невозможен)
		 * При успешном переходе к следующему эл-ту списка формируется событие ListActorEvent.PREV_ITEM
		 */
		public function prevItem():void
		{
			if (itemIndex - 1 < 0)
			{
				dispatchEvent(new ListActorEvent(ListActorEvent.TOP_OF_LIST));
				return;
			}
			else
			if (itemIndex - 1 == 0)
				dispatchEvent(new ListActorEvent(ListActorEvent.FIRST_ITEM));
			itemIndex--;
			currentItem = itemsList.getItemAt(itemIndex) as ListItem;
			dispatchEvent(new ListActorEvent(ListActorEvent.PREV_ITEM));
		}
		override public function setData(data:DataObject):void
		{
			if (data == null)
				return;
			var tmp:DataListActorObject = data as DataListActorObject;
			itemsList = tmp.itemsList;
			if (tmp.itemsList.length == 0)
			{
				currentItem = null;
				itemIndex = 0;
				return;
			}
			if(tmp.currentItem)
				currentItem = tmp.currentItem;
			else
				currentItem = itemsList.getItemAt(0) as ListItem;
			if (tmp.itemIndex)
				itemIndex = tmp.itemIndex;
			else
				itemIndex = 0;
		}
		override public function getData():DataObject
		{
			var tmp:DataListActorObject = new DataListActorObject();
			tmp.currentItem = currentItem;
			tmp.itemIndex = itemIndex;
			tmp.itemsList = itemsList;
			return tmp;
		}
	}

}