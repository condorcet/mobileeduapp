package ru.segrys.actors.DataActors 
{
	import flash.display.Bitmap;
	import flash.net.URLRequest;
	import mx.collections.ArrayList;
	import ru.segrys.application.Loaders.Adapters.ImageLoaderAdapter;
	import ru.segrys.Do.DataListActorObject;
	import ru.segrys.Do.ListItem.ChaptersItemDataObject;
	import ru.segrys.Do.ListOfChaptersDataObject;
	import ru.segrys.Do.DataObject;
	import ru.segrys.events.ActorEvent.DataActors.ChaptersEvent;
	import ru.segrys.application.Caches.ApplicationCacheFacade;
	import ru.segrys.application.Loaders.ApplicationLoadersFacade;
	import ru.segrys.events.loader.LoaderEvent;
	/**
	 * Модель данных списка разделов
	 * @author Novikov
	 */
	public class ListOfChaptersActor extends AbstractListActor 
	{
		
		[Inject]
		public var loaderFacade:ApplicationLoadersFacade;
		
		[Inject]
		public var cacheFacade:ApplicationCacheFacade;
		
		/**
		 * URL на раздел
		 */
		private var chapterURL:String;
		
		/**
		 * Список разделов
		 */
		
		public function ListOfChaptersActor() 
		{
			
		}
		override public function setData(data:DataObject):void
		{
			super.setData(data as ListOfChaptersDataObject);
			eventDispatcher.dispatchEvent(new ChaptersEvent(ChaptersEvent.CHAPTERS_LOADED));
		}
		
		override public function getData():DataObject
		{
			var tmp:ListOfChaptersDataObject = new ListOfChaptersDataObject(super.getData() as DataListActorObject);
			return tmp;
		}
		
		public function getImages():void
		{
			//Реализация некрасивая, старался добиться "параллельности" загрузки изображений
			for each(var key:ChaptersItemDataObject in list.source)
			{
			//var key:ChaptersItemDataObject = list.getItemAt(0) as ChaptersItemDataObject;
				var loader:ImageLoaderAdapter = new ImageLoaderAdapter(loaderFacade, cacheFacade);
				addListeners(loader, key);
				loader.load(new URLRequest((key as ChaptersItemDataObject).imageURL));
				
				function addListeners(loader:ImageLoaderAdapter, key:Object):void
				{
					loader.addEventListener(LoaderEvent.COMPLETE, completeHandler);
					loader.addEventListener(LoaderEvent.ERROR, errorHandler);
					function completeHandler(e:LoaderEvent):void
					{
						loader.removeEventListener(LoaderEvent.COMPLETE, completeHandler);
						loader.removeEventListener(LoaderEvent.ERROR, errorHandler);
						(key as ChaptersItemDataObject).imageData = loader.data as Bitmap;
						loader.destroy();
					}
					function errorHandler(e:LoaderEvent):void
					{
						loader.removeEventListener(LoaderEvent.COMPLETE, completeHandler);
						loader.removeEventListener(LoaderEvent.ERROR, errorHandler);
						trace('error while loading image');
						loader.destroy();
					}
				}
				
			}
		}
	}

}