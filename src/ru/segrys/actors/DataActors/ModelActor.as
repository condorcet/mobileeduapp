package ru.segrys.actors.DataActors 
{
	import flash.display.DisplayObject;
	import flash.net.URLRequest;
	import ru.segrys.application.Caches.ApplicationCacheFacade;
	import ru.segrys.application.Loaders.Adapters.SWFLoaderAdapter;
	import ru.segrys.application.Loaders.ApplicationLoadersFacade;
	import ru.segrys.Do.ModelDataObject;
	import ru.segrys.Do.DataObject;
	import ru.segrys.events.ActorEvent.DataActors.ModelEvent;
	import ru.segrys.events.loader.LoaderEvent;
	/**
	 * Класс модели данных визуальной модели
	 * @author Novikov
	 */
	public class ModelActor extends AbstractActor 
	{
		
		[Inject]
		public var loaderFacade:ApplicationLoadersFacade;
		
		[Inject]
		public var cacheFacade:ApplicationCacheFacade;
		
		/**
		 * URL на визуальную модель
		 */
		[Bindable]
		public var modelURL:String;
		/**
		 * Отображаемый объект
		 */
		private var modelObject:DisplayObject;
		/**
		 * Заголовок
		 */
		private var header:String;
		/**
		 * Описание
		 */
		private var description:String;
		
		public function ModelActor()
		{
			
		}
		
		override public function getData():DataObject
		{
			var tmp:ModelDataObject = new ModelDataObject();
			tmp.modelURL = modelURL;
			tmp.modelObject = modelObject;
			tmp.header = header;
			tmp.description = description;
			return tmp;
		}
		override public function setData(data:DataObject):void
		{
			var modelData:ModelDataObject = data as ModelDataObject;
			modelURL = modelData.modelURL;
			modelObject = modelData.modelObject;
			header = modelData.header;
			description = modelData.description;
		}
		
		public function get model():DisplayObject
		{
			return modelObject;
		}
		
		public function loadModel():void
		{
			var modelLoader:SWFLoaderAdapter = new SWFLoaderAdapter(loaderFacade, cacheFacade);
			modelLoader.addEventListener(LoaderEvent.COMPLETE, completeLoadModel);
			modelLoader.addEventListener(LoaderEvent.ERROR, errorLoadModel);
			modelLoader.load(new URLRequest(modelURL));
			function completeLoadModel(e:LoaderEvent):void
			{
				modelObject = modelLoader.data as DisplayObject;
				modelLoader.destroy();
				dispatchEvent(new ModelEvent(ModelEvent.MODEL_LOADED, modelURL));
			}	
			function errorLoadModel(e:LoaderEvent):void
			{
				dispatchEvent(new ModelEvent(ModelEvent.MODEL_LOADING_ERROR, modelURL));
				trace('error while loading model');
				modelLoader.destroy();
			}
		}
	}

}