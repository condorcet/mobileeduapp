package ru.segrys.actors.DataActors 
{
	import ru.segrys.Do.DataObject;
	
	/**
	 * ...
	 * @author Novikov
	 */
	public interface IActor 
	{
		function setData(data:DataObject):void;
		function getData():DataObject;
	}
	
}