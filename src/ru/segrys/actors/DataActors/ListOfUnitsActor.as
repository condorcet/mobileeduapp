package ru.segrys.actors.DataActors 
{
	import ru.segrys.Do.ListItem.UnitsItemDataObject;
	import ru.segrys.Do.DataObject;
	import ru.segrys.Do.DataListActorObject
	import ru.segrys.Do.ListOfUnitsDataObject;
	import ru.segrys.events.ActorEvent.DataActors.UnitsEvent;
	import ru.segrys.application.Loaders.ApplicationLoadersFacade
	import ru.segrys.application.Caches.ApplicationCacheFacade;
	import ru.segrys.application.Loaders.Adapters.ImageLoaderAdapter;
	import flash.net.URLRequest;
	import ru.segrys.events.loader.LoaderEvent;
	import flash.display.Bitmap;
	/**
	 * Класс модели списка дидактических единиц
	 * @author Novikov
	 */
	public class ListOfUnitsActor extends AbstractListActor 
	{
		[Inject]
		public var loaderFacade:ApplicationLoadersFacade;
		
		[Inject]
		public var cacheFacade:ApplicationCacheFacade;
		
		public function ListOfUnitsActor() 
		{
			
		}
		
		override public function getData():DataObject
		{
			var tmp:ListOfUnitsDataObject = new ListOfUnitsDataObject(super.getData() as DataListActorObject);
			return tmp;
		}
		
		override public function setData(data:DataObject):void
		{
			super.setData(data as ListOfUnitsDataObject);
			eventDispatcher.dispatchEvent(new UnitsEvent(UnitsEvent.UNITS_LOADED));
		}
		
		public function getImages():void
		{
			//Реализация некрасивая, старался добиться "параллельности" загрузки изображений
			for each(var key:UnitsItemDataObject in list.source)
			{
				var loader:ImageLoaderAdapter = new ImageLoaderAdapter(loaderFacade, cacheFacade);
				addListeners(loader, key);
				loader.load(new URLRequest((key as UnitsItemDataObject).imageURL));
				
				function addListeners(loader:ImageLoaderAdapter, key:Object):void
				{
					loader.addEventListener(LoaderEvent.COMPLETE, completeHandler);
					loader.addEventListener(LoaderEvent.ERROR, errorHandler);
					function completeHandler(e:LoaderEvent):void
					{
						loader.removeEventListener(LoaderEvent.COMPLETE, completeHandler);
						loader.removeEventListener(LoaderEvent.ERROR, errorHandler);
						(key as UnitsItemDataObject).imageData = loader.data as Bitmap;
						loader.destroy();
					}
					function errorHandler(e:LoaderEvent):void
					{
						loader.removeEventListener(LoaderEvent.COMPLETE, completeHandler);
						loader.removeEventListener(LoaderEvent.ERROR, errorHandler);
						trace('error while loading image');
						loader.destroy();
					}
				}
				
			}
		}
	}

}