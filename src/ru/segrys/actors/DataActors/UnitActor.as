package ru.segrys.actors.DataActors 
{
	import flash.events.IEventDispatcher;
	import ru.segrys.Do.ListOfModelsDataObject;
	import ru.segrys.Do.TheoryDataObject;
	import ru.segrys.Do.UnitActorDataObject;
	import ru.segrys.Do.DataObject;
	import ru.segrys.events.ActorEvent.DataActors.UnitsEvent;
	/**
	 * Класс модели данных дидактической единицы
	 * @author Novikov
	 */
	public class UnitActor extends AbstractActor 
	{
		/**
		 * Заголовок
		 */
		private var header:String;
		/**
		 * Описание
		 */
		private var description:String;
		/**
		 * URL ссылка на дидактическую единицу
		 */
		public var unitURL:String;
		/**
		 * URL на раздел
		 */
		public var chapterURL:String;
		//[Inject]??
		/**
		 * Ссылка на теорию
		 */
		[Inject]
		public var theoryActor:TheoryActor;
		//[Inject]??
		/**
		 * Ссылка на список моделей дидактической единицы
		 */
		[Inject]
		public var listOfModelsActor:ListOfModelsActor;
				
		public function UnitActor() 
		{
			
		}
		
		override public function getData():DataObject
		{
			var tmp:UnitActorDataObject = new UnitActorDataObject();
			tmp.header = header;
			tmp.description = description;
			tmp.listOfModels = listOfModelsActor.getData() as ListOfModelsDataObject;
			tmp.theory = theoryActor.getData() as TheoryDataObject;
			return tmp;
		}
		
		override public function setData(data:DataObject):void
		{
			var tmp:UnitActorDataObject = data as UnitActorDataObject;
			header = tmp.header;
			description = tmp.description;
			listOfModelsActor.setData(tmp.listOfModels as DataObject);
			theoryActor.setData(tmp.theory as DataObject);
			eventDispatcher.dispatchEvent(new UnitsEvent(UnitsEvent.UNIT_LOADED));
		}
	}

}