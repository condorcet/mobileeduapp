package ru.segrys.parser 
{
	import mx.collections.ArrayList;
	import ru.segrys.actors.DataActors.AbstractListActor;
	import ru.segrys.Do.ChapterDataObject;
	import ru.segrys.Do.ListItem.UnitsItemDataObject;
	import ru.segrys.Do.ListOfUnitsDataObject;
	/**
	 * ...
	 * @author Novikov
	 */
	public class ChapterParser extends AbstractParser 
	{
		
		public function ChapterParser() 
		{
			super();
			
		}
		
		override protected function parseData():Boolean
		{
			var array:ArrayList = new ArrayList();
			var chapterXML:XML = data as XML;
			if (!chapterXML)
				return false;
			var header:String = chapterXML.header;
			var description:String = chapterXML.description;
			for each (var key:XML in chapterXML.child('units').unit)
			{
				var item:UnitsItemDataObject = new UnitsItemDataObject();
				item.header = key.@header;
				item.url = key.@url;
				item.imageURL = key.@image;
				array.addItem(item);
			}
			var dataObject:ChapterDataObject = new ChapterDataObject();
			dataObject.description = description;
			dataObject.header = header;
			var tmpList:ListOfUnitsDataObject = new ListOfUnitsDataObject();
			tmpList.list = array;
			dataObject.listOfUnits = tmpList;
			result = dataObject;
			return true;
		}
		
	}

}