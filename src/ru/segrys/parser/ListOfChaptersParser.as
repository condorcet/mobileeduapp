package ru.segrys.parser 
{
	import mx.collections.ArrayList;
	import ru.segrys.actors.DataActors.ListOfChaptersActor;
	import ru.segrys.Do.ListOfChaptersDataObject;
	import ru.segrys.Do.ListItem.ChaptersItemDataObject;
	/**
	 * ...
	 * @author ...
	 */
	public class ListOfChaptersParser extends AbstractParser 
	{
		
		public function ListOfChaptersParser() 
		{
			super();
		}
		override protected function parseData():Boolean
		{
			var array:ArrayList = new ArrayList();
			var list:XMLList = (data as XML).child('chapter');
			for each(var key:* in list)
			{
				var tmp:ChaptersItemDataObject = new ChaptersItemDataObject();
				tmp.url = key.@url;
				tmp.description = key.@description;
				tmp.imageURL = key.@image;
				tmp.header = key.@header;
				array.addItem(tmp);
			}
			var listOfChapters:ListOfChaptersDataObject = new ListOfChaptersDataObject();
			listOfChapters.itemsList = array;
			result = listOfChapters;
			return true;
		}
	}

}