package ru.segrys.parser 
{
	import flash.events.EventDispatcher;
	import ru.segrys.Do.DataObject;
	import ru.segrys.events.parser.ParserEvent;
	/**
	 * Класс парсера данных.
	 * Поле data содержит данные для парсинга.
	 * Поле result содержит результаты парсинга.
	 * @author Novikov
	 */
	
	/**
	 * Событие формируется в случае окончания парсинга
	 * @eventType ru.segrys.events.parser.ParserEvent.COMPLETE
	 */
	[Event(name="parseComplete",type="ru.segrys.events.parser.ParserEvent")]
	 
	public class AbstractParser extends EventDispatcher implements IParser 
	{
		/**
		 * Данные для парсинга
		 */
		public var data:Object;
		/**
		 * Результаты парсинга
		 */
		public var result:DataObject;
		
		public function AbstractParser() 
		{
			
		}
		/**
		 * Метод для парсинга данных
		 */
		public function parse():void
		{
			if(parseData())
				dispatchEvent(new ParserEvent(ParserEvent.COMPLETE));
		}
		/**
		 * Непосредственно алгоритм парсинга данных. Метод должен быть переопределен в наследниках
		 */
		protected function parseData():Boolean
		{
			throw new Error('must overridden');
			return false;
		}
		
	}

}