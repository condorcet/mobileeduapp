package ru.segrys.parser 
{
	import mx.collections.ArrayList;
	import ru.segrys.Do.ListItem.ModelsItemDataObject
	import ru.segrys.Do.ListOfModelsDataObject;
	import ru.segrys.Do.UnitActorDataObject;
	/**
	 * ...
	 * @author Novikov
	 */
	public class UnitParser extends AbstractParser 
	{
		
		public function UnitParser() 
		{
			super();
			
		}
		
		override protected function parseData():Boolean
		{
			var modelsArray:ArrayList = new ArrayList();
			var unitXML:XML = data as XML;
			for each (var key:XML in unitXML.child('models').model)
			{
				var item:ModelsItemDataObject = new ModelsItemDataObject();
				item.header = key.@header;
				item.id = key.@id;
				item.modelURL = key.@url;
				modelsArray.addItem(item);
			}
			var listOfModels:ListOfModelsDataObject = new ListOfModelsDataObject();
			listOfModels.list = modelsArray;
			var unitDataObject:UnitActorDataObject = new UnitActorDataObject();
			unitDataObject.header = unitXML.header;
			unitDataObject.listOfModels = listOfModels;
			unitDataObject.description = null; //временно!
			unitDataObject.theory = null; //временно!
			result = unitDataObject;
			return true;
		}
		
	}

}