package ru.segrys.Do 
{
	/**
	 * Класс модели данных дидактической единицы
	 * @author Novikov
	 */
	public class UnitActorDataObject extends DataObject 
	{
		/**
		 * Заголовок
		 */
		public var header:String;
		/**
		 * Описание
		 */
		public var description:String;
		/**
		 * Ссылка на модель данных теории
		 */
		public var theory:TheoryDataObject;
		/**
		 * Ссылка на модель данных списка виз.моделей
		 */
		public var listOfModels:ListOfModelsDataObject;
		
		public function UnitActorDataObject() 
		{
			
		}
		
	}

}