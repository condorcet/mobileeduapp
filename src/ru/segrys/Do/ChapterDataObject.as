package ru.segrys.Do 
{
	/**
	 * Класс данных модели раздела
	 * @see ru.segrys.actors.ChapterActor
	 * @author Novikov
	 */
	public class ChapterDataObject extends DataObject 
	{
		/**
		 * Заголовок
		 */
		public var header:String;
		/**
		 * Описание
		 */
		public var description:String;
		/**
		 * Список дидактических единиц
		 */
		public var listOfUnits:ListOfUnitsDataObject;
		/**
		 * URL на раздел
		 */
		public var chapterURL:String;
		
		public function ChapterDataObject() 
		{
			
		}
		
	}

}