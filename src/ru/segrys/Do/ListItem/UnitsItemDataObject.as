package ru.segrys.Do.ListItem 
{
	import flash.display.Bitmap;
	/**
	 * ...
	 * @author Novikov
	 */
	public class UnitsItemDataObject extends ListItem
	{
		[Bindable]
		public var url:String;
		[Bindable]
		public var header:String;
		[Bindable]
		public var imageURL:String;
		[Bindable]
		public var imageData:Bitmap;
		
		public function UnitsItemDataObject() 
		{
			super();
		}
		
	}

}