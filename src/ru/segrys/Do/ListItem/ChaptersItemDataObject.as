package ru.segrys.Do.ListItem 
{
	import flash.display.Bitmap;
	/**
	 * ...
	 * @author ...
	 */
	public class ChaptersItemDataObject extends ListItem 
	{
		[Bindable]
		public var url:String;
		[Bindable]
		public var header:String;
		[Bindable]
		public var description:String;
		[Bindable]
		public var imageURL:String;
		[Bindable]
		public var imageData:Bitmap;
		
		public function ChaptersItemDataObject()
		{
			super();
		}
		
	}

}