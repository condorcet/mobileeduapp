package ru.segrys.Do.ListItem 
{
	import flash.display.DisplayObject;
	/**
	 * ...
	 * @author Novikov
	 */
	public class ModelsItemDataObject extends ListItem 
	{
		[Bindable]
		public var header:String;
		
		[Bindable]
		public var modelURL:String;
		
		[Bindable]
		public var modelData:DisplayObject;
		
		public var id:String;
		
		public function ModelsItemDataObject() 
		{
			super();
			
		}
		
	}

}