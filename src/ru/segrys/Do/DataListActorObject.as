package ru.segrys.Do 
{
	import mx.collections.ArrayList;
	import ru.segrys.Do.ListItem.ListItem;
	/**
	 * ...
	 * @author Novikov
	 */
	public class DataListActorObject extends DataObject 
	{
		public var currentItem:ListItem; //текущий элемент
		public var itemsList:ArrayList; //список элементов
		public var itemIndex:uint; //указатель на текущий элемент в списке
		
		public function DataListActorObject(dataObject:DataListActorObject = null) 
		{
			if (dataObject != null)
			{
				currentItem = dataObject.currentItem;
				itemsList = dataObject.itemsList;
				itemIndex = dataObject.itemIndex;
			}
		}
		
		public function set list(val:ArrayList):void
		{
			itemsList = val;
		}
		public function get list():ArrayList
		{
			return itemsList;
		}
		
	}

}