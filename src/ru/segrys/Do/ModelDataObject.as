package ru.segrys.Do 
{
	import flash.display.DisplayObject;
	/**
	 * Класс данных модели визуальной модели
	 * @author Novikov
	 */
	public class ModelDataObject extends DataObject 
	{
		/**
		 * URL на модель
		 */
		public var modelURL:String;
		/**
		 * Заголовок
		 */
		public var header:String;
		/**
		 * Описание
		 */
		public var description:String;
		/**
		 * Ссылка на объект виз.модели
		 */
		public var modelObject:DisplayObject;
		
		public function ModelDataObject() 
		{
			
		}
		
	}

}