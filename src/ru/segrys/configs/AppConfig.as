package ru.segrys.configs 
{
	import flash.events.IEventDispatcher;
	import robotlegs.bender.extensions.eventCommandMap.api.IEventCommandMap;
	import robotlegs.bender.extensions.mediatorMap.api.IMediatorMap;
	import robotlegs.bender.framework.api.IConfig;
	import robotlegs.bender.framework.api.IInjector;
	import ru.segrys.application.Loaders.ApplicationLoadersFacade;
	import ru.segrys.application.Caches.ApplicationCacheFacade;
	
	//ACTORS
	import ru.segrys.actors.DataActors.ChapterActor;
	import ru.segrys.actors.DataActors.ListOfChaptersActor;
	import ru.segrys.actors.DataActors.ListOfModelsActor;
	import ru.segrys.actors.DataActors.ListOfUnitsActor;
	import ru.segrys.actors.DataActors.ModelActor;
	import ru.segrys.actors.DataActors.TheoryActor;
	import ru.segrys.actors.DataActors.UnitActor;
	
	//EVENTS
	import ru.segrys.events.Application.ApplicationEvent;
	import ru.segrys.events.Application.Cache.CacheFacadeErrorEvent;
	import ru.segrys.events.ActorEvent.DataActors.ChaptersEvent;
	import ru.segrys.events.ActorEvent.DataActors.UnitsEvent;
	
	//COMMANDS
	import ru.segrys.commands.ApplicationCommand.*;
	import ru.segrys.commands.CachesCommand.*;
	import ru.segrys.commands.ChaptersCommand.*;
	import ru.segrys.commands.UnitsCommand.*;
	
	//VIEWS
	import ru.segrys.views.MainV.* ;
	import ru.segrys.views.Chapters.*;
	import ru.segrys.views.Units.*;
	import ru.segrys.views.Unit.*;
	import ru.segrys.views.*;

	/**
	 * ...
	 * @author Novikov
	 */
	public class AppConfig implements IConfig 
	{
		[Inject]
		public var injector:IInjector
		[Inject]
		public var mediatorMap:IMediatorMap
		[Inject]
		public var commandMap:IEventCommandMap;
		[Inject]
		public var dispatcher:IEventDispatcher;
		
		public function AppConfig() 
		{
			
		}
		
		public function configure():void
		{
			configureFacades();
			configureActors();
			configureCommands();
			configureMediators();
			start();
		}
		
		private function configureFacades():void
		{
			injector.map(ApplicationCacheFacade).asSingleton();
			injector.map(ApplicationLoadersFacade).asSingleton();
		}
		
		private function configureActors():void
		{
			injector.map(ChapterActor).asSingleton();
			injector.map(ListOfChaptersActor).asSingleton();
			injector.map(ListOfModelsActor).asSingleton();
			injector.map(ListOfUnitsActor).asSingleton();
			injector.map(ModelActor).asSingleton();
			injector.map(TheoryActor).asSingleton();
			injector.map(UnitActor).asSingleton();
		}
		
		private function configureCommands():void
		{
			commandMap.map(ApplicationEvent.APPLICATION_STARTING).toCommand(ApplicationStartingCommand);
			commandMap.map(ApplicationEvent.APPLICATION_STARTED).toCommand(ApplicationStartedCommand);
			commandMap.map(ApplicationEvent.APPLICATION_STARTING_ERROR).toCommand(ApplicationStartingErrorCommand);
			
			commandMap.map(CacheFacadeErrorEvent.CACHE_NOT_EXISTS).toCommand(CacheNotExistsCommand);
			
			commandMap.map(ChaptersEvent.LOAD_CHAPTERS).toCommand(LoadChaptersCommand);
			commandMap.map(ChaptersEvent.LOAD_CHAPTER).toCommand(LoadChapterCommand);
			commandMap.map(ChaptersEvent.GO_TO_CHAPTER).toCommand(GoToChapterCommand);
			
			commandMap.map(UnitsEvent.GO_TO_UNIT).toCommand(GoToUnitCommand);
			commandMap.map(UnitsEvent.LOAD_UNIT).toCommand(LoadUnitCommand);
			commandMap.map(UnitsEvent.LOAD_UNIT_MODEL).toCommand(LoadUnitModelCommand);
			
		}
		
		private function start():void
		{
			dispatcher.dispatchEvent(new ApplicationEvent(ApplicationEvent.APPLICATION_STARTING));
		}
		
		private function configureMediators():void
		{
			mediatorMap.map(Main).toMediator(ApplicationMediator);
			mediatorMap.map(MainView).toMediator(MainViewMediator);
			mediatorMap.map(ChaptersView).toMediator(ChaptersViewMediator);
			mediatorMap.map(UnitsView).toMediator(UnitsViewMediator);
			mediatorMap.map(UnitView).toMediator(UnitViewMediator);
		}
	}

}