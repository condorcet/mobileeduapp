package ru.segrys.views 
{
	import robotlegs.bender.bundles.mvcs.Mediator;
	import robotlegs.bender.extensions.contextView.ContextView;
	import spark.components.ViewNavigatorApplication;
	
	//VIEW
	import ru.segrys.views.*;
	import ru.segrys.views.MainV.*;
	import ru.segrys.views.Chapters.*;
	import ru.segrys.views.Units.*;
	import ru.segrys.views.Unit.*;
	//EVENTS
	import ru.segrys.events.Application.ApplicationEvent;	
	import ru.segrys.events.ActorEvent.DataActors.ChaptersEvent;
	import ru.segrys.events.ActorEvent.DataActors.UnitsEvent;
	/**
	 * ...
	 * @author Novikov
	 */
		
	public class ApplicationMediator extends Mediator 
	{
		[Inject]
		public var contextView:ContextView;
		
		

		public function ApplicationMediator() 
		{

		}
		override public function initialize():void
		{
			//eventMap.mapListener(eventDispatcher, ApplicationEvent.APPLICATION_STARTED);
			eventMap.mapListener(eventDispatcher, ChaptersEvent.LOAD_CHAPTERS, gotoChapters);
			eventMap.mapListener(eventDispatcher, ChaptersEvent.GO_TO_CHAPTER, gotoChapter);
			eventMap.mapListener(eventDispatcher, UnitsEvent.GO_TO_UNIT, gotoUnit);
			//ViewNavigatorApplication(contextView.view).navigator.pushView(MainView);
		}
		
		private function gotoChapters(e:ChaptersEvent):void
		{
			ViewNavigatorApplication(contextView.view).navigator.pushView(ChaptersView);
		}
		
		private function gotoChapter(e:ChaptersEvent):void
		{
			ViewNavigatorApplication(contextView.view).navigator.pushView(UnitsView);
		}
		
		private function gotoUnit(e:UnitsEvent):void
		{
			ViewNavigatorApplication(contextView.view).navigator.pushView(UnitView);
		}
	}

}