package ru.segrys.views.Unit 
{
	import mx.collections.ArrayList;
	import robotlegs.bender.bundles.mvcs.Mediator;
	import ru.segrys.actors.DataActors.ListOfUnitsActor;
	import ru.segrys.actors.DataActors.ModelActor;
	import ru.segrys.actors.DataActors.UnitActor;
	import ru.segrys.application.Caches.ApplicationCacheFacade;
	import ru.segrys.application.Loaders.Adapters.ImageLoaderAdapter;
	import ru.segrys.application.Loaders.ApplicationLoadersFacade;
	import ru.segrys.events.ActorEvent.DataActors.ChaptersEvent;
	import ru.segrys.events.ActorEvent.DataActors.UnitsEvent;
	import ru.segrys.events.ActorEvent.DataActors.ModelEvent;
	import ru.segrys.params.AppParams;
	/**
	 * ...
	 * @author Novikov
	 */
	public class UnitViewMediator extends Mediator 
	{
		[Inject]
		public var view:UnitView;
		
		[Inject]
		public var model:UnitActor;
		
		[Inject]
		public var modelActor:ModelActor;
		
		
		public function UnitViewMediator() 
		{
			super();
			
		}
		override public function initialize():void
		{
			eventMap.mapListener(eventDispatcher, UnitsEvent.UNIT_LOADED, dataUpdatedHandler);
			eventMap.mapListener(eventDispatcher, ModelEvent.MODEL_LOADED, modelUpdateHandler);
			if (view.model == null)
				dataUpdated();
		}
		
		private function dataUpdated():void
		{
			//ЭТО НЕПРАВИЛЬНО! ТАК ДЕЛАТЬ НЕЛЬЗЯ
			view.model = AppParams.SERVER_URL + modelActor.modelURL;
		}
		
		private function modelUpdateHandler(e:ModelEvent):void
		{
			dataUpdated();
		}
		
		private function dataUpdatedHandler(e:UnitsEvent):void
		{
			dataUpdated();
		}
	}

}