package ru.segrys.views.Units 
{
	import mx.collections.ArrayList;
	import robotlegs.bender.bundles.mvcs.Mediator;
	import ru.segrys.actors.DataActors.ListOfUnitsActor;
	import ru.segrys.application.Caches.ApplicationCacheFacade;
	import ru.segrys.application.Loaders.Adapters.ImageLoaderAdapter;
	import ru.segrys.application.Loaders.ApplicationLoadersFacade;
	import ru.segrys.events.ActorEvent.DataActors.ChaptersEvent;
	import ru.segrys.events.ActorEvent.DataActors.UnitsEvent;
	/**
	 * ...
	 * @author Novikov
	 */
	public class UnitsViewMediator extends Mediator 
	{
		[Inject]
		public var view:UnitsView;
		
		[Inject]
		public var model:ListOfUnitsActor;
				
		public function UnitsViewMediator() 
		{
			super();
			
		}
		override public function initialize():void
		{
			addViewListener(UnitsEvent.GO_TO_UNIT, dispatch);
			eventMap.mapListener(eventDispatcher, UnitsEvent.UNITS_LOADED, dataUpdatedHandler);
			if (model.list != null)
				dataUpdated();
		}
				
		private function dataUpdated():void
		{
			model.getImages();
			view.list = model.list;
		}
		
		private function dataUpdatedHandler(e:UnitsEvent):void
		{
			dataUpdated();
		}
	}

}