package views 
{
	import robotlegs.bender.bundles.mvcs.Mediator;
	import ru.segrys.events.Application.ApplicationEvent;
	import robotlegs.bender.extensions.contextView.ContextView;
	import spark.components.ViewNavigatorApplication;
	/**
	 * ...
	 * @author Novikov
	 */
	public class NavigationViewMediator extends Mediator 
	{
		[Inject]
		public var contextView:ContextView;
		
		public function NavigationViewMediator() 
		{
			
		}
		override public function initialize():void
		{
			
			addViewListener(ApplicationEvent.APPLICATION_EXITING, dispatch);
		}
		
		private function goToHome(e:AppContextEvent):void
		{
			ViewNavigatorApplication(contextView.view).navigator.popToFirstView();
		}
		
	}

}