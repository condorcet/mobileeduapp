package ru.segrys.views.Chapters 
{
	import mx.collections.ArrayList;
	import robotlegs.bender.bundles.mvcs.Mediator;
	import ru.segrys.actors.DataActors.ListOfChaptersActor;
	import ru.segrys.application.Caches.ApplicationCacheFacade;
	import ru.segrys.application.Loaders.Adapters.ImageLoaderAdapter;
	import ru.segrys.application.Loaders.ApplicationLoadersFacade;
	import ru.segrys.events.ActorEvent.DataActors.ChaptersEvent;
	/**
	 * ...
	 * @author Novikov
	 */
	public class ChaptersViewMediator extends Mediator 
	{
		[Inject]
		public var view:ChaptersView;
		
		[Inject]
		public var model:ListOfChaptersActor;
				
		public function ChaptersViewMediator() 
		{
			super();
			
		}
		override public function initialize():void
		{
			addViewListener(ChaptersEvent.GO_TO_CHAPTER, dispatch);
			eventMap.mapListener(eventDispatcher, ChaptersEvent.CHAPTERS_LOADED, dataUpdatedHandler);
			if(model.list != null)
				dataUpdated();
		}
		
		private function dataUpdated():void
		{
			model.getImages();
			view.list = model.list;
		}
		
		private function dataUpdatedHandler(e:ChaptersEvent):void
		{
			dataUpdated();
		}
	}

}