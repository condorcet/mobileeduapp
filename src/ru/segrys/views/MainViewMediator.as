package ru.segrys.views 
{
	import mx.collections.ArrayList;
	import robotlegs.bender.bundles.mvcs.Mediator;
	import robotlegs.bender.extensions.contextView.ContextView;
	import ru.segrys.events.ActorEvent.DataActors.ChaptersEvent;
	/**
	 * ...
	 * @author Novikov
	 */
	public class MainViewMediator extends Mediator 
	{
		[Inject]
		public var view:MainView;
		[Inject]
		public var contextView:ContextView;
		
		public var counter:uint = 0;
		
		override public function initialize():void
		{
			//eventDispatcher.dispatchEvent(new AppEvent(AppEvent.READY));
			addViewListener(ChaptersEvent.LOAD_CHAPTERS, loadChaptersHandler);
		}
		
		private function loadChaptersHandler(e:ChaptersEvent):void
		{
			eventDispatcher.dispatchEvent(e);
		}
		
	/*	private function dataLoaded(e:ListEvent):void
		{

		}*/
		/*private function loadItemHandler(e:ItemEvent):void
		{
			eventDispatcher.dispatchEvent(e);
		}*/
	}

}