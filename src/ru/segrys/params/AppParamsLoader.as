package ru.segrys.params 
{
	import flash.events.EventDispatcher;
	import ru.segrys.events.Application.Params.ParamsLoaderEvent;
	/**
	 * Класс загрузчика параметров приложения
	 * @author Novikov
	 */
	/** 
	* Событие будет вызвано, когда параметры приложения успешно загружены
	* @eventType ru.segrys.events.Application.Params.ParamsLoaderEvent.PARAMS_LOADED
	*/ 
	[Event(name = "paramsLoaded", type = "ru.segrys.events.Application.Params.ParamsLoaderEvent")]
	public class AppParamsLoader extends EventDispatcher
	{
		private static var instance:AppParamsLoader;
		
		public function AppParamsLoader(pvt:PrivateClass) 
		{
		}
		
		public static function getInstance():AppParamsLoader
		{
			if (!instance)
				instance = new AppParamsLoader(new PrivateClass());
			return instance;
		}
		
		public function loadData():void
		{
			//загрузка файла по адресу AppParams.SETTINGS_FILE
			dispatchEvent(new ParamsLoaderEvent(ParamsLoaderEvent.PARAMS_LOADED));
		}
		
	}

}

class PrivateClass
{
	public function PrivateClass()
	{
		
	}
}