package ru.segrys.params 
{
	/**
	 * Класс параметров приложения
	 * @author Novikov
	 */
	public class AppParams 
	{
		
		public function AppParams() 
		{
			
		}
		/**
		 * Режим работы приложения (см. константы)
		 */
		public static var mode:String = ONLINE;
		/**
		 * Флаг кэширования данных в приложении
		 */
		public static var cachable:Boolean = false;
		/**
		 * URL сервера
		 */
		public static const SERVER_URL:String = 'res\\';//'http://project.test/';//; 
		/**
		 * Путь к файлу с параметрами приложения
		 */
		public static const SETTINGS_FILE:String = 'settings.xml';
		/**
		 * Онлайн режим работы
		 */
		public static const ONLINE:String = 'online';
		/**
		 * Автономный режим работы
		 */
		public static const OFFLINE:String = 'offline';
		
		/**
		 * Ссылка на список разделов
		 */
		public static const chaptersURL:String = 'Chapters.xml';

	}

}