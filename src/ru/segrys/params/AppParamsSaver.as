package ru.segrys.params 
{
	import flash.events.EventDispatcher;
	/**
	 * Класс сохранения данных приложения
	 * @author Novikov
	 */
	public class AppParamsSaver extends EventDispatcher
	{
		private static var instance:AppParamsSaver;
		
		public function AppParamsSaver(pvt:PrivateClass) 
		{
			
		}
		public static function getInstance():AppParamsSaver
		{
			if (!instance)
				instance = new AppParamsSaver(new PrivateClass());
			return instance;
		}
		
		public function saveData():void
		{
			//данные из AppParams сохра-ся в файл AppParams.SETTINGS_FILE
		}
		

	}

}

class PrivateClass
{
	public function PrivateClass()
	{
		
	}
}