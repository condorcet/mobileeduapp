package ru.segrys.application.Loaders.Fabrics
{
	import ru.segrys.application.Loaders.Adapters.ItemLoaderAdapter;
	
	/**
	 * ...
	 * @author Novikov
	 */
	public interface ILoaderAdapter 
	{
		function getLoader():ItemLoaderAdapter;
	}
	
}