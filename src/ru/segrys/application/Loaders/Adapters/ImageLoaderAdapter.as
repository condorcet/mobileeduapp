package ru.segrys.application.Loaders.Adapters 
{
	import ru.segrys.application.Caches.ApplicationCacheFacade;
	import ru.segrys.application.Loaders.ApplicationLoadersFacade;
	import ru.segrys.application.Caches.CacheFabrics;
	import ru.segrys.loaders.ImageItemLoader;
	
	/**
	 * ...
	 * @author Novikov
	 */
	public class ImageLoaderAdapter extends ItemLoaderAdapter 
	{
		
		public function ImageLoaderAdapter(loaderFacade:ApplicationLoadersFacade, cacheFacade:ApplicationCacheFacade) 
		{
			cacheClass = CacheFabrics.IMAGE;
			loaderClass = ImageItemLoader;
			super(loaderFacade, cacheFacade);
			
		}
		
	}

}