package ru.segrys.application.Loaders.Adapters 
{
	import ru.segrys.application.Caches.CacheFabrics;
	import ru.segrys.loaders.SWFItemLoader;
	import ru.segrys.application.Loaders.ApplicationLoadersFacade;
	import ru.segrys.application.Caches.ApplicationCacheFacade;
	
	/**
	 * ...
	 * @author ...
	 */
	
	 
	public class SWFLoaderAdapter extends ItemLoaderAdapter 
	{
		
		public function SWFLoaderAdapter(loaderFacade:ApplicationLoadersFacade, cacheFacade:ApplicationCacheFacade) 
		{
			cacheClass = CacheFabrics.SWFs;
			loaderClass = SWFItemLoader;
			super(loaderFacade, cacheFacade);
		}
		
	}

}