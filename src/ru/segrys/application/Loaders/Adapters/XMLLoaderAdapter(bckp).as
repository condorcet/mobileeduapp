package ru.segrys.application.Loaders.Adapters 
{
	import ru.segrys.application.Caches.CacheFabrics;
	import ru.segrys.loaders.XMLItemLoader;
	/**
	 * ...
	 * @author ...
	 */
	
	 
	public class XMLLoaderAdapter extends ItemLoaderAdapter 
	{
		
		public function XMLLoaderAdapter() 
		{
			cacheClass = CacheFabrics.XMLs;
			loaderClass = XMLItemLoader;
			super();
		}
		
	}

}