package ru.segrys.application.Loaders.Adapters 
{
	import ru.segrys.application.Caches.CacheFabrics;
	import ru.segrys.loaders.XMLItemLoader;
	import ru.segrys.application.Loaders.ApplicationLoadersFacade;
	import ru.segrys.application.Caches.ApplicationCacheFacade;
	
	/**
	 * ...
	 * @author ...
	 */
	
	 
	public class XMLLoaderAdapter extends ItemLoaderAdapter 
	{
		
		public function XMLLoaderAdapter(loaderFacade:ApplicationLoadersFacade, cacheFacade:ApplicationCacheFacade) 
		{
			cacheClass = CacheFabrics.XMLs;
			loaderClass = XMLItemLoader;
			super(loaderFacade, cacheFacade);
		}
		
	}

}