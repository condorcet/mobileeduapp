package ru.segrys.application.Loaders.Adapters 
{
	import ru.segrys.application.Loaders.ApplicationLoadersFacade;
	import ru.segrys.caches.AbstractCache;
	import ru.segrys.caches.ICache;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.net.URLRequest;
	import ru.segrys.loaders.ItemLoader;
	import flash.events.IOErrorEvent;
	import ru.segrys.application.Caches.ApplicationCacheFacade;
	import ru.segrys.events.cache.CacheEvent
	import ru.segrys.events.cache.ItemCacheEvent;
	import ru.segrys.events.Application.Loader.ApplicationLoaderFacadeEvent;
	/**
	 * Класс-адапетр загрузчиков. Включает в себя ссылки на загрузчик данных и кэш данных
	 * @author Novikov
	 */
	public class ItemLoaderAdapter extends EventDispatcher
	{
		/**
		 * Ссылка на класс-фасад управления кэшами
		 * @see ru.segrys.application.Caches.ApplicationCacheFacade
		 */
		[Inject]
		public var cacheFacade:ApplicationCacheFacade;
		/**
		 * Ссылка на класс-фасад управления загрузчиками
		 * @see ru.segrys.application.Loaders.ApplicationLoadersFacade
		 */
		[Inject]
		public var loaderFacade:ApplicationLoadersFacade;
		
		/**
		 * Ссылка на кэш данных
		 */
		protected var cache:AbstractCache;
		/**
		 * Ссылка на загрузчик данных
		 */
		protected var loader:ItemLoader;
		/**
		 * Ссылка на класс кэша данных
		 */
		protected var cacheClass:Class;
		/**
		 * Ссылка на класс загрузчика данных
		 */
		protected var loaderClass:Class;
		
		public function ItemLoaderAdapter()
		{
			init();
			
			loader.addEventListener(Event.COMPLETE, loaderComplete);
			loader.addEventListener(IOErrorEvent.IO_ERROR, loaderError);
			
			cache.addEventListener(CacheEvent.CACHETABLE_ERROR, cacheTableErrorMessage);
			cache.addEventListener(ItemCacheEvent.ITEM_NOT_SAVED, itemSaveErrorMessage);
			cache.addEventListener(CacheEvent.CACHE_NOT_EXISTS, cacheErrorMessage);
			
			//по окончанию работы с loader-ом вызвать метод destroy !!
			loaderFacade.addEventListener(ApplicationLoaderFacadeEvent.CACHING_ENABLED, cachingEnabled);
			loaderFacade.addEventListener(ApplicationLoaderFacadeEvent.CACHING_DISABLED, cachingDisabled);
		}
		/**
		 * Загрузить данные
		 * @param	url объект класса URLRequest
		 */
		public function load(url:URLRequest):void
		{
			loader.load(url);
		}
		/**
		 * Загруженные данные
		 */
		public function get data():Object
		{
			return loader.data;
		}
		/**
		 * Использованный при загрузке запрос
		 */
		public function get request():URLRequest
		{
			return loader.request;
		}
		/**
		 * Кэшируются ли данные
		 */
		public function get cachable():Boolean
		{
			return loader.cachable;
		}
		/**
		 * @private
		 */
		public function set cachable(value:Boolean):void
		{
			loader.cachable = value;
		}
		/**
		 * Ссылка на загрузчик используемый в адаптере
		 * @return загрузчик
		 * @see ru.segrys.loaders.ItemLoader
		 */
		public function getLoader():ItemLoader
		{
			return loader;
		}
		/**
		 * Ссылка на кэш используемые в адаптере
		 * @return кэш
		 * @see ru.segrys.caches.ICache
		 * @see ru.segrys.caches.AbstractCache
		 */
		public function getCache():ICache
		{
			return cache;
		}
		/**
		 * Удалить загрузчик
		 * Адаптер слушает события класса-фасада AplicationLoaderFacade, поэтому обязательно следует вызвывать destroy, если адаптер больше не используется!
		 * @see ru.segrys.application.Loaders.ApplicationLoadersFacade
		 */
		public function destroy():void
		{
			loaderFacade.removeEventListener(ApplicationLoaderFacadeEvent.CACHING_ENABLED, cachingEnabled);
			loaderFacade.removeEventListener(ApplicationLoaderFacadeEvent.CACHING_DISABLED, cachingDisabled);
			loader = null;
			cache = null;
			loaderFacade = null;
			cacheFacade = null;
		}
		
		protected function init():void
		{
			if (!cacheClass)
				throw new Error('cache-class not defined');
			if (!loaderClass)
				throw new Error('loader-class not defined');
			cache = cacheFacade.getCache(cacheClass) as AbstractCache;
			cache.loadCache();
			loader = new loaderClass(loaderFacade.cachable, cache);
		}
		
		protected function cacheTableErrorMessage(e:CacheEvent):void
		{
			//здесь описать действия, при возникновении ошибки
			//eventDispatcher.dispatchEvent(new LoaderAdapterError(LoaderAdapterError.CACHE_TABLE, this);
		}
		
		protected function itemSaveErrorMessage(e:CacheEvent):void
		{
			//здесь описать действия, при возникновении ошибки
			//аналогично см. выше
		}
		
		protected function cacheErrorMessage(e:CacheEvent):void
		{
			//здесь описать действия, при возникновении ошибки	
			//аналогично см. выше
		}
		
		protected function loaderComplete(e:Event):void
		{
			dispatchEvent(e);
		}
		protected function loaderError(e:Event):void
		{
			dispatchEvent(e);
		}
		
		private function cachingEnabled(e:ApplicationLoaderFacadeEvent):void
		{
			loader.cachable = true;
		}
		
		private function cachingDisabled(e:ApplicationLoaderFacadeEvent):void
		{
			loader.cachable = false;
		}
	}

}