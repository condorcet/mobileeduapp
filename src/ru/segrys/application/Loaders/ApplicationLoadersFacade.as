package ru.segrys.application.Loaders 
{
	import flash.events.EventDispatcher;
	import ru.segrys.events.Application.Loader.ApplicationLoaderFacadeEvent;
	import ru.segrys.params.AppParams;
	/**
	 * Класс-фасад предназначенный для управления адаптерами загрузчиков.
	 * Адаптеры загрузчиков подписываются на события фасада
	 * @see ru.segrys.application.Loaders.Adapters.ItemLoaderAdapter
	 * @author Novikov
	 */
	/**
	 * Событие формируются когда кэширование данных включено
	 * @eventType ru.segrys.events.Application.Loader.ApplicationLoaderFacadeEvent.CACHING_ENABLED
	 */
	[Event(name = "cachingEnable", type = "ru.segrys.events.Application.Loader.ApplicationLoaderFacadeEvent")]
	/**
	 * Событие формируется когда кэширование данных отключено
	 * @eventType ru.segrys.events.Application.Loader.ApplicationLoaderFacadeEvent.CACHING_DISABLED
	 */
	[Event(name="cachingDisable", type="ru.segrys.events.Application.Loader.ApplicationLoaderFacadeEvent")]
	 	
	public class ApplicationLoadersFacade extends EventDispatcher
	{
		private var _cachable:Boolean;
		
		public function ApplicationLoadersFacade() 
		{
			trace('loaders facade init');
		}
		/**
		 * Кэширование данных
		 */
		public function get cachable():Boolean
		{
			return _cachable;
		}
		/**
		 * @private
		 */
		public function set cachable(value:Boolean):void
		{
			_cachable = value;
			if (value)
				dispatchEvent(new ApplicationLoaderFacadeEvent(ApplicationLoaderFacadeEvent.CACHING_ENABLED));
			else
				dispatchEvent(new ApplicationLoaderFacadeEvent(ApplicationLoaderFacadeEvent.CACHING_DISABLED));
		}
		
		/**
		 * URL на сервер
		 */
		public function get root():String
		{
			return AppParams.SERVER_URL;
		}
	}

}