package ru.segrys.application.Parsers 
{
	import flash.events.EventDispatcher;
	import ru.segrys.actors.DataActors.AbstractActor;
	import ru.segrys.Do.DataObject;
	import ru.segrys.parser.AbstractParser;
	import ru.segrys.events.parser.ParserEvent;
	
	/**
	 * Класс-адаптер парсера данных
	 * @author Novikov
	 */
	/**
	 * Событие формируются когда парсинг успешно выполнен
	 * @eventType ru.segrys.events.parser.ParserEvent.COMPLETE
	 */
	[Event(name = "parseComplete", type = "ru.segrys.events.parser.ParserEvent")]
	/**
	 * Событие формируется когда парсинг не выполнил (произошла ошибка)
	 * @eventType ru.segrys.events.parser.ParserEvent.ERROR
	 */
	[Event(name = "parseError", type = "ru.segrys.events.parser.ParserEvent")]
	
	public class AbstractParserAdapter extends EventDispatcher 
	{
		private var parser:AbstractParser;
		//в наследниках actor будет inject-иться
		private var actor:AbstractActor;
		
		public function AbstractParserAdapter() 
		{
			parser.addEventListener(ParserEvent.COMPLETE, parseComplete);
			parser.addEventListener(ParserEvent.ERROR, parseError);
		}
		/**
		 * Спарсить данные хранящиеся в поле data
		 */
		public function parse():void
		{
			parser.parse();
		}
		/**
		 * Результат парсинга
		 */
		public function get result():DataObject
		{
			return parser.result;
		}
		/**
		 * Данные для парсинга
		 */
		public function set data(value:Object):void
		{
			parser.data = value;
		}
		/**
		 * @private
		 */
		public function get data():Object
		{
			return parser.data;
		}
		
		private function parseComplete(e:ParserEvent):void
		{
			dispatchEvent(new ParserEvent(ParserEvent.COMPLETE));
			//actor.setData(parser.result); ТАКОГО БОЛЬШЕ НЕТ
		}
		
		private function parseError(e:ParserEvent):void
		{
			//действия в случае ошибки
			dispatchEvent(new ParserEvent(ParserEvent.ERROR));
		}
	}

}