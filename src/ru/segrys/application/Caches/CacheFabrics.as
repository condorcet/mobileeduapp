package ru.segrys.application.Caches 
{
	import ru.segrys.application.Caches.Fabrics.*;
	/**
	 * Класс представляет существующие кэши приложения, и ссылки на их классы (см. статические поля)
	 * @author Novikov
	 */
	public class CacheFabrics 
	{
		
		public function CacheFabrics() 
		{
			caches = [IMAGE, XMLs, SWFs];
		}
		
		public var caches:Array;
		
		static public const IMAGE:Class = ImageCacheFabric; //ссылка на класс фабрики, в которой создается кэш изображений
		static public const XMLs:Class = XMLCacheFabric;
		static public const SWFs:Class = SWFCacheFabric;
	}

}