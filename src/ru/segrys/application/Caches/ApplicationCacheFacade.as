package ru.segrys.application.Caches 
{
	import flash.events.IEventDispatcher;
	import flash.utils.Dictionary;
	import ru.segrys.caches.AbstractCache;
	import ru.segrys.caches.ICache;
	import ru.segrys.events.Application.Cache.CacheFacadeErrorEvent;
	import ru.segrys.events.cache.CacheEvent;
	//import ru.segrys.application.Caches.Fabrics.ImageCacheFabric;
	import ru.segrys.application.Caches.Fabrics.ICacheFabric;
	import ru.segrys.application.Caches.CacheFabrics;
	/**
	 * Класс-фасад. Обеспечивает единую точку управления кэшами в рамках приложения
	 * @author Novikov
	 */
	public class ApplicationCacheFacade 
	{
		public function ApplicationCacheFacade() 
		{
			openCaches = new Dictionary();
			caches = new CacheFabrics();
		}
		/**
		 * Возвращает объект кэша
		 * @param	name класс возвращаемого кэша (см. статические методы соответствуюших класс-фабрик)
		 * @return
		 */
		public function getCache(name:Class):ICache
		{
			var fabric:ICacheFabric;
			fabric = new name as ICacheFabric;
			if (!openCaches[name])
			{
				openCaches[name] = fabric.getCache();
				(openCaches[name] as AbstractCache).addEventListener(CacheEvent.CACHE_NOT_EXISTS, cacheErrorHandler);
				(openCaches[name] as ICache).loadCache();
			}
			return fabric.getCache();// создание экземпляра кэша фабрикой
			
			function cacheErrorHandler(e:CacheEvent):void
			{
				(fabric.getCache() as AbstractCache).removeEventListener(CacheEvent.CACHE_NOT_EXISTS, cacheErrorHandler);
				eventDispatcher.dispatchEvent(new CacheFacadeErrorEvent(CacheFacadeErrorEvent.CACHE_NOT_EXISTS, fabric));
			}
		}
		/**
		 * Очистка кэша
		 * @param	name класс очищаемого кэша (см. статические методы соответствуюших класс-фабрик)
		 */
		public function clearCache(name:Class):void
		{
			//очистка содержимого кэша name
			getCache(name).clearCache();
		}
		/**
		 * Очистка всех кэшей
		 */
		public function clearCaches():void
		{
			//очистка содержимого всех кэшей
			for each (var key:Class in caches.caches)
			{
				clearCache(key);
			}
		}
		/**
		 * Обновление всех активированных кэшей
		 */
		public function updateCaches():void
		{
			for each(var key:Class in openCaches)
			{
				getCache(key).updateCache();
			}
		}
		
		public function createCache(fabric:ICacheFabric):void
		{
			var cache:AbstractCache = fabric.getCache() as AbstractCache;
			cache.addEventListener(CacheEvent.CREATED, cacheCreated);
			cache.createCache();
			function cacheCreated(e:CacheEvent):void
			{
				cache.removeEventListener(CacheEvent.CREATED, cacheCreated);
				cache.loadCache();
			}
		}
		
		/**
		 * Загрузка всех кэшей
		 */
		public function loadCaches():void
		{
			for each (var key:Class in caches.caches)
			{
				getCache(key);
			}
		}
		
		/**
		 * 
		 */
		
		public function getCaches():Array
		{
			var list:Array = new Array();
			for each (var key:Class in caches.caches)
			{
				list.push(getCache(key));
			}
			return list;
		}
		 
		/**
		 * Количество кэшей существующих в приложении
		 */
		public function get numberOfCaches():uint
		{
			return caches.caches.length;
		}
		
		/*public function get listOfCaches():Array
		{
			return caches.caches;
		}*/
		
		[Inject]
		public var eventDispatcher:IEventDispatcher;
		
		/**
		 * Активированные (открытые) кэши
		 */
		private var openCaches:Dictionary;
		/**
		 * Ссылка на кэш фабрики
		 */
		private var caches:CacheFabrics;
	}

}