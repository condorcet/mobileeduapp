package ru.segrys.application.Caches.Fabrics 
{
	import ru.segrys.caches.FSCache;
	import ru.segrys.caches.ICache;
	import flash.filesystem.File;
	/**
	 * ...
	 * @author ...
	 */
	public class XMLCacheFabric implements ICacheFabric 
	{
		private static var instance:FSCache;
		
		private static var pathToCache:String = 'D:\\cache\\XMLs';
		
		private static var pathToCacheTable:String = 'D:\\cache\\xmls_cache.tbl';
		
		public function XMLCacheFabric() 
		{
			
		}
		
		/* INTERFACE ru.segrys.application.Caches.Fabrics.ICacheFabric */
		
		public function getCache():ICache
		{
			if (!instance)
			{
				instance = new FSCache(new File(pathToCache), new File(pathToCacheTable));
			}
			return instance;
		}
		
	}

}