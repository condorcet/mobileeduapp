package ru.segrys.application.Caches.Fabrics 
{
	import ru.segrys.caches.FSCache;
	import ru.segrys.caches.ICache;
	import flash.filesystem.File;
	/**
	 * Класс-фабрика кэша картинок. Является также синглтоном
	 * @author Novikov
	 */
	
	public class ImageCacheFabric implements ICacheFabric 
	{
		private static var instance:FSCache;
		/**
		 * Путь до директории кэша
		 */
		private static var pathToCache:String = 'D:\\cache\\images';
		/**
		 * Путь до кэш-таблицы
		 */
		private static var pathToCacheTable:String = 'D:\\cache\\images_cache.tbl';
		public function ImageCacheFabric() 
		{
			
		}
		public function getCache():ICache
		{
			if (!instance)
			{
				instance = new FSCache(new File(pathToCache), new File(pathToCacheTable));
			}
			return instance;
		}
		
	}

}