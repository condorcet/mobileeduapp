package ru.segrys.application.Caches.Fabrics 
{
	import ru.segrys.caches.FSCache;
	import ru.segrys.caches.ICache;
	import flash.filesystem.File
	/**
	 * ...
	 * @author Novikov
	 */
	public class SWFCacheFabric implements ICacheFabric 
	{
		private static var instance:FSCache;
		
		private static var pathToCache:String = 'D:\\cache\\SWFs';
		
		private static var pathToCacheTable:String = 'D:\\cache\\swfs_cache.tbl';
		
		public function SWFCacheFabric() 
		{
			
		}
		
		/* INTERFACE ru.segrys.application.Caches.Fabrics.ICacheFabric */
		
		public function getCache():ICache 
		{
			if (!instance)
			{
				instance = new FSCache(new File(pathToCache), new File(pathToCacheTable));
			}
			return instance;
		}
		
	}

}